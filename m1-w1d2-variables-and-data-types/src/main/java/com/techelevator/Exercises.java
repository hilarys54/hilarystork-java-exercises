package com.techelevator;

public class Exercises {

	public static void main(String[] args) {
        
        /* 
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch? 
        */
		int birdsOnBranch = 4;
		int birdsFlewAway = 1;
		int birdsLeft = birdsOnBranch - birdsFlewAway;
		System.out.println("Birds left on the branch: " + birdsLeft);

		int birdsOnBranch = 4;
		int birdsFlewAway = 1;
		int birdsLeftOnBranch = birdsOnBranch - birdsFlewAway;
		System.out.println("Birds left on the branch: " + birdsLeftOnBranch);
		
		
        /* 
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests? 
        */
		
		int birds = 6;
		int nests = 3;
		int extraBirds = birds - nests;
		System.out.println("Extra birds: " + extraBirds);

        /* 
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods? 
        */

		int racoonsPlayingInWoods = 3;
		int raccoonsLeftForDinner = 2;
		int raccoonsLeftInTheWoods = racoonsPlayingInWoods - raccoonsLeftForDinner;
		System.out.println("Racoons left playing in the woods: " + raccoonsLeftInTheWoods);
		
        /* 
        4. There are 5 flowers and 3 bees. How many less bees than flowers? 
        */

		int flowers = 5;
		int bees = 3;
		int lessBeesThanFlowers = flowers - bees;
		System.out.println("Less bees than flowers: " + lessBeesThanFlowers);
		
        /* 
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now? 
        */

		int lonelyPigeon1 = 1;
		int lonelyPigeon2 = 1;
		int totalLonelyPigeons = lonelyPigeon1 + lonelyPigeon2;
		System.out.println("Total lonely pigeons: " + totalLonelyPigeons);
		
        /* 
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now? 
        */

		int originalOwls = 3;
		int newOwls = 2;
		int totalOwls = originalOwls + newOwls;
		System.out.println("Total owls: " + totalOwls);
		
        /* 
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home? 
        */

		int beaversWorkingOnHome = 2;
		int beaversGoneForSwim = 1;
		int beaversStillWorkingOnHome = beaversWorkingOnHome - beaversGoneForSwim;
		System.out.println("Beavers still working on home: " + beaversStillWorkingOnHome);
		
        /* 
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all? 
        */

		int originalToucans = 2;
		int newToucans = 1;
		int totalToucans = originalToucans + newToucans;
		System.out.println("Total toucans: " + totalToucans);
		
        /* 
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts? 
        */

		int squirrels = 4;
		int nuts = 2;
		int moreSquirrelsThanNuts = squirrels - nuts;
		System.out.println("More squirrels than nuts: " + moreSquirrelsThanNuts);
		
        /* 
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find? 
        */

		int quarter = 25;
		int dime = 10;
		int nickel = 5;
		int totalMoney = quarter + dime + 2*nickel;
		System.out.println("Total money found: " + totalMoney + " cents");
		
        /* 
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all? 
        */

		int briersMuffins = 18;
		int macadamsMuffins = 20;
		int flannerysMuffins = 17;
		int firstGradeMuffins = briersMuffins + macadamsMuffins + flannerysMuffins;
		System.out.println("Total first grade muffins: " + firstGradeMuffins);
		
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
        
		int priceYoyo = 25;
		int priceWhistle = 14;
		int costOfToys = priceYoyo + priceWhistle;
		System.out.println("Cost of toys: " + costOfToys + " cents");
		
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
        
		int largeMarshmallows = 8;
		int miniMarshmallows = 10;
		int totalMarshmallows = largeMarshmallows + miniMarshmallows;
		System.out.println("Total marshmallows: " + totalMarshmallows);
		
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
        
		int hiltsSnow = 29;
		int brecknockSnow = 17;
		int moreSnowAtHilts = hiltsSnow + brecknockSnow;
		System.out.println("How much more snow at Hilt's: " + moreSnowAtHilts + " inches");
		
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
        
		int mrsHiltsMoney = 10;
		int costOfToyTruck = 3;
		int costOfPencil = 2;
		int mrsHiltsRemainingMoney = mrsHiltsMoney - costOfToyTruck - costOfPencil;
		System.out.println("Mrs. Hilt's remaining money: " + mrsHiltsRemainingMoney + " dollars");
		
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
        
		int collectionMarbles = 16;
		int lostMarbles = 7;
		int remainingMarbles = collectionMarbles - lostMarbles;
		System.out.println("Remaining marbles: " + remainingMarbles);
		
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
        
		int megansSeashells = 19;
		int totalSeashells = 25;
		int seashellsNeeded = totalSeashells - megansSeashells;
		System.out.println("Seashells needed: " + seashellsNeeded);
		
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
         
		int totalBalloons = 17;
		int redBalloons = 8;
		int greenBalloons = totalBalloons - redBalloons;
		System.out.println("Green balloons: " + greenBalloons);
		
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
        
		int currentBooksOnShelf = 38;
		int newBooksOnShelf = 10;
		int totalBooksOnShelf = currentBooksOnShelf + newBooksOnShelf;
		System.out.println("Total books on shelf: " + totalBooksOnShelf);
		
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
        
		int beeLegs = 6;
		int totalBeeLegs = 8*beeLegs;
		System.out.println("Total bee legs: " + totalBeeLegs);
		
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
        
		int costOfIceCreamCone = 99;
		int totalCostOfCones = 2*costOfIceCreamCone;
		System.out.println("Total cost of ice cream cones: " + totalCostOfCones + " cents");
		
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
        
		int totalRocksForBorder = 125;
		int hiltsCurrentRocks = 64;
		int hiltsNeededRocks = totalRocksForBorder - hiltsCurrentRocks;
		System.out.println("MRs. Hilt's needed rocks: " + hiltsNeededRocks);
		
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
        
		int hiltsOriginalMarbles = 38;
		int hiltsLostMarbles = 15;
		int hiltsLeftMarbles = hiltsOriginalMarbles - hiltsLostMarbles;
		System.out.println("Mrs. Hilt's marbles left: " + hiltsLeftMarbles);
		
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
        
		int distanceToConcert = 78;
		int milesDrivenAlready = 32;
		int milesLeftToGo = distanceToConcert - milesDrivenAlready;
		System.out.println("Miles left to go: " + milesLeftToGo);
		
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
        
		int saturdayShovelingTime = 90;
		int sundayShovelingTime = 45;
		int weekendShovelingTime = saturdayShovelingTime + sundayShovelingTime;
		System.out.println("Total shoveling time: " + weekendShovelingTime + " minnutes");
		
        /*    
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
        
		int numberHotDogs = 6;
		int costOfHotDog = 50;
		int totalCostOf6Dogs = numberHotDogs * costOfHotDog;
		System.out.println("Total cost of 6 hotdogs: " + totalCostOf6Dogs + " cents");
		
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
        
		int mrsHiltsPencilMoney = 50;
		int hiltsCostOfPencil = 7;
		int numberOfPencils = mrsHiltsPencilMoney / hiltsCostOfPencil;
		System.out.println("Number of pencils: " + numberOfPencils);
		
        /*    
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
        
		int totalButterflies = 33;
		int orangeButterflies = 20;
		int redButterflies = totalButterflies - orangeButterflies;
		System.out.println("Red butterflies: " + redButterflies);
		
        /*    
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
        
		int katesOriginalMoney = 100;
		int costOfCandy = 54;
		int katesMoneyBack = katesOriginalMoney - costOfCandy;
		System.out.println("Kate's money back: " + katesMoneyBack);
		
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
        
		int marksOriginalTrees = 13;
		int marksNewTrees = 12;
		int marksTotalTrees = marksOriginalTrees + marksNewTrees;
		System.out.println("Mark's total trees: " + marksOriginalTrees);
		
        /*    
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
        
		int numberOfDaysUntilGrandma = 2;
		int numberHoursInDay = 24; 
		int numberOfHoursUntilGrandma = numberOfDaysUntilGrandma * numberHoursInDay;
		System.out.println("Hours until Joy sees Grandma: " + numberOfHoursUntilGrandma);
		
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
        
		int numberOfKimsCousins = 4;
		int numberOfPiecesOfGum = 5;
		int totalGum = numberOfKimsCousins * numberOfPiecesOfGum;
		System.out.println("Total pieces of gum: " + totalGum);
		
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
        
		int dansOriginalMoney = 3;
		int costOfDansCandyBar = 1;
		int dansMoneyLeft = dansOriginalMoney - costOfDansCandyBar;
		System.out.println("Dan's money left: " + dansMoneyLeft);
		
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
        
		int boatsOnLake = 5;
		int peoplePerBoat = 3;
		int totalPeopleOnBoats = boatsOnLake * peoplePerBoat;
		System.out.println("Total people on boats: " + totalPeopleOnBoats);
		
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
        
		int ellensOriginalLegos = 380;
		int ellensLostLegos = 57;
		int ellensTotalLegos = ellensOriginalLegos - ellensOriginalLegos;
		System.out.println("Ellen's total legos: " + ellensTotalLegos);
		
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
        
		int arthursBakedMuffins = 35;
		int arthursTotalMuffins = 83;
		int arthursNeededMuffins = arthursTotalMuffins - arthursBakedMuffins;
		System.out.println("Arthur's Needed Muffins: " + arthursNeededMuffins);
		
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
        
		int willysCrayons = 1400;
		int lucysCrayons = 290;
		int howManyMoreCrayonsWillyHasThanLucy = willysCrayons - lucysCrayons;
		System.out.println("How many more crayons Willy has than Lucy: " + howManyMoreCrayonsWillyHasThanLucy);
		
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
        
		int stickersPerPage = 10;
		int numberOfPages = 22;
		int totalStickers = stickersPerPage * numberOfPages;
		System.out.println("Total stickers: " + totalStickers);
		
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
        
		int totalCupcakes = 96;
		int children = 8;
		int cupcakesPerChild = totalCupcakes / children;
		System.out.println("Cupcakes per child: " + cupcakesPerChild);
		
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
        
		int totalGingerbreadCookies = 47;
		int cookiesPerJar = 6;
		int remainderOfCookies = totalGingerbreadCookies % cookiesPerJar;
		System.out.println("Cookies not placed in a jar: " + remainderOfCookies);
		
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
        
		int totalCroissants = 59;
		int numberOfNeighbors = 8;
		int numberOfCroissantsLeft = totalCroissants % numberOfNeighbors;
		System.out.println("Number of croissants left with Marian: " + numberOfCroissantsLeft);
		
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
         
		int cookiesPerTray = 12;
		int totalCookies = 276;
		int numberOfTrays = totalCookies / cookiesPerTray;
		System.out.println("Number of trays: " + numberOfTrays);
		
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
        
		int totalPretzels = 480;
		int oneServingPretzels = 12;
		int totalServingsOfPretzels = totalPretzels / oneServingPretzels;
		System.out.println("Servings of bite-sized pretzels: " + totalServingsOfPretzels);
		
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
        
		int totalLemonCupcakes = 53;
		int cupcakesLeftAtHome = 2;
		int cupcakesPerBox = 3;
		int numberOfBoxes = (totalLemonCupcakes - cupcakesLeftAtHome) / cupcakesPerBox;
		System.out.println("Number of boxes of lemon cupcakes given away: " + numberOfBoxes);
		
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
        
		int carrotsForBreakfast = 74;
		int numberOfPeople = 12;
		int uneatenCarrots = carrotsForBreakfast % numberOfPeople;
		System.out.println("Number of uneaten carrots: " + uneatenCarrots);
		
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
        
		int totalTeddyBears = 98;
		int bearsPerShelf = 7;
		int shelvesFilled = totalTeddyBears / bearsPerShelf;
		System.out.println("Shelves filled with teddy bears: " + shelvesFilled);
		
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
        
		int totalPictures = 480;
		int picturesPerAlbum = 20;
		int totalAlbums = totalPictures / picturesPerAlbum;
		System.out.println("Total number of albums: " + totalAlbums);
		
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
        
		int joesTotalCards = 94;
		int cardsPerBox = 8;
		int boxesFilled = 94 / 8;
		int cardsInUnfilledBox = 94 % 8;
		System.out.println("Number of boxes filled: " + boxesFilled + ", Number of cards in unfilled box: " + cardsInUnfilledBox);
		;
		
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */

		int dadsTotalBooks = 210;
		int numberOfShelves = 10;
		int booksPerRepairedShelf = dadsTotalBooks / numberOfShelves;
		System.out.println("Number of books on each shelf: " + booksPerRepairedShelf);
		
        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
		
		int cristinasTotalCroissants = 17;
		int cristinasGuests = 7;
		int wholeCroissantsPerGuest = cristinasTotalCroissants / cristinasGuests;
		System.out.println("Cristina's croissants per guest: " + wholeCroissantsPerGuest);
            
	}

}
