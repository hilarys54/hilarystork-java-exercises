package com.techelevator.company;

public class CompanyTest {

	public static void main(String[] args) {
		
		// write code here that verifies the functionality of the Company class
		
		boolean allTestsPassed = true;
		
		Company company = new Company();
		String name = "Awesome Company";
		company.setName(name);
		if(company.getName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No name found.");
		} else if (!name.equals(company.getName())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Names don't match.");
		}
		int numberOfEmployees = 20;
		if (company.getNumberOfEmployees() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees initialized incorrectly.");
		}
		company.setNumberOfEmployees(numberOfEmployees);
		if (company.getNumberOfEmployees() != numberOfEmployees) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees doesn't match.");
		}
		String companySize = "small";
		if (!companySize.equals(company.getCompanySize())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Company size doesn't match.");
		}
		numberOfEmployees = 100;
		
		company.setNumberOfEmployees(numberOfEmployees);
		if (company.getNumberOfEmployees() != numberOfEmployees) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees doesn't match.");
		}
		companySize = "medium";
		if (!companySize.equals(company.getCompanySize())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Comapny size doesn't match.");
		}
		numberOfEmployees = 300;
		
		company.setNumberOfEmployees(numberOfEmployees);
		if (company.getNumberOfEmployees() != numberOfEmployees) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees doesn't match.");
		}
		companySize = "large";
		if (!companySize.equals(company.getCompanySize())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Comapny size doesn't match.");
		}
		if(company.getRevenue() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Revenue initialized incorrectly.");
		}
		double revenue = 100;
		company.setRevenue(revenue);
		if (revenue != company.getRevenue()) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Revenues don't match.");
		}
		if(company.getExpenses() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Expenses initialized incorrectly.");
		} 
		double expenses = 80;
		company.setExpenses(expenses);
		if (expenses != company.getExpenses()) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Expenses don't match.");
		}
		double profit = revenue - expenses;
		
		if (profit != company.getProfit()) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Profits don't match.");
		}
		
		if (allTestsPassed) {
			System.out.println("All tests PASSED!!!");
		}
	}
}
		

