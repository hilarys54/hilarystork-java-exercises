package com.techelevator.person;

import com.techelevator.company.Company;

public class PersonTest {

	public static void main(String[] args) {
		
		// write code here that verifies the functionality of the Person class
	
	boolean allTestsPassed = true;
		
		Person person = new Person();
		
		String firstName = "Hilary";
		person.setFirstName(firstName);
		if(person.getFirstName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No first name found.");
		} else if (!firstName.equals(person.getFirstName())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: First names don't match.");
		}
	
		String lastName = "Stork";
		person.setLastName(lastName);
		if(person.getLastName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No last name found.");
		} else if (!lastName.equals(person.getLastName())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Last names don't match.");
		}
		String fullName = firstName + lastName;
		if (!fullName.equals(person.getFullName())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Full names don't match.");
		}
		int age = 36;
		if (person.getAge() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Age initialized incorrectly.");
		}
		person.setAge(age);
		if (person.getAge() != age) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Age doesn't match.");
		}
		person.setAge(36);
		if (!person.isAdult()) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Adult status doesn't match.");
		}
		if (allTestsPassed) {
			System.out.println("All tests PASSED!!!");
		}
	}
}
