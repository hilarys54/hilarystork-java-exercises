package com.techelevator.product;

public class ProductTest {

	public static void main(String[] args) {
		
		// write code here that verifies the functionality of your Product class
	
	boolean allTestsPassed = true;
		
		Product product = new Product();
		
		String name = "Awesome Product";
		product.setName(name);
		if(product.getName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No name found.");
		} else if (!name.equals(product.getName())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Names don't match.");	
		
		}
		double price = 100.00;
		if (product.getPrice() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Price initialized incorrectly.");
		}
		product.setPrice(price);
		if (product.getPrice() != price) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Price doesn't match.");
		}
		int weightInOunces = 20;
		if (product.getWeightInOunces() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Weight in ounces initialized incorrectly.");
		}
		product.setWeightInOunces(weightInOunces);
		if (product.getWeightInOunces() != weightInOunces) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Weight in ounces doesn't match.");
		}
		if (allTestsPassed) {
			System.out.println("All tests PASSED!!!");
		}	
	}
}
