package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AirplaneTest {
	private Airplane ourPlane;

	@Before
	public void setup() {
		ourPlane = new Airplane("200300", 13, 150);	
	}
	
	
	@Test
	public void initialization_values_are_stored_correctly() {
		
		int firstClassSeats = ourPlane.getAvailableFirstClassSeats();
		int coachSeats = ourPlane.getAvailableCoachSeats();
		
		Assert.assertEquals(13, firstClassSeats);
		Assert.assertEquals(150, coachSeats);
	}
	
	@Test
	public void reserve_a_first_class_seat() {
		boolean success = ourPlane.reserve(true, 1);
		
		int firstClassAvailable = ourPlane.getAvailableFirstClassSeats();
		
		Assert.assertTrue("We were not able to reserve a first class seat", success);
		Assert.assertEquals(12, firstClassAvailable);		
	}
	
	@Test
	public void reserve_a_coach_seat() {
		boolean success = ourPlane.reserve(false, 1);
		
		int coachAvailable = ourPlane.getAvailableCoachSeats();
		
		Assert.assertTrue("Failed to reserve a coach seat", success);
		Assert.assertEquals(149, coachAvailable);
	}
	
	@Test
	public void reserve_too_many_first_class_seats() {
		boolean success = ourPlane.reserve(true, 15);
		
		int firstClassAvailable = ourPlane.getAvailableFirstClassSeats();
		
		Assert.assertFalse("We were able to reserve more first class seats than the plane has.", success);
		Assert.assertEquals(13, firstClassAvailable);
	}
	
	@Test
	public void plane_number_is_stored_correctly() {
		
		String planeNumber = ourPlane.getPlaneNumber();
		
		Assert.assertEquals("200300", planeNumber);
			
	}

	@Test
	public void booked_first_class_seats_is_stored_correctly() {
		
		int bookedFirstClassSeats = ourPlane.getBookedFirstClassSeats();
		
		Assert.assertEquals(0, bookedFirstClassSeats);
			
	}
	
	@Test
	public void total_first_class_seats_is_stored_correctly() {
		
		int totalFirstClassSeats = ourPlane.getTotalFirstClassSeats();
		
		Assert.assertEquals(13, totalFirstClassSeats);
	}
	
	@Test
	public void total_coach_seats_is_stored_correctly() {
		
		int totalCoachSeats = ourPlane.getTotalCoachSeats();
		
		Assert.assertEquals(150, totalCoachSeats);
	}
	
	@Test
	public void booked_coach_seats_is_stored_correctly() {
		
		int bookedCoachSeats = ourPlane.getBookedCoachSeats();
		
		Assert.assertEquals(0, bookedCoachSeats);
			
	}
	
	@Test
	public void reserve_too_many_coach_seats() {
		boolean success = ourPlane.reserve(false, 200);
		
		int coachAvailable = ourPlane.getAvailableCoachSeats();
		
		Assert.assertFalse("We were able to reserve more coach seats than the plane has.", success);
		Assert.assertEquals(150, coachAvailable);
	}
}
