package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ElevatorTest {
	private Elevator ourElevator;

	@Before
	public void setup() {
		ourElevator = new Elevator(3, 20) ;
	}
	
	
	@Test
	public void initialization_values_are_stored_correctly() {
		int shaftNumber = ourElevator.getShaftNumber();
		int numberOfLevels = ourElevator.getNumberOfLevels();
		int currentLevel = ourElevator.getCurrentLevel();
		
		Assert.assertEquals(3, shaftNumber);
		Assert.assertEquals(20, numberOfLevels);
		Assert.assertEquals(1, currentLevel);
	}
	
	@Test
	public void get_door_open_works() {
		boolean isDoorOpen = ourElevator.getIsDoorOpen();
		Assert.assertFalse(isDoorOpen);	
	}
	
	@Test
	public void does_open_door_work() {
		ourElevator.OpenDoor();
		boolean doorOpen = ourElevator.getIsDoorOpen();
		Assert.assertTrue(doorOpen);
	}
	
	@Test
	public void does_close_door_work() {
		ourElevator.CloseDoor();
		boolean doorOpen = ourElevator.getIsDoorOpen();
		Assert.assertFalse(doorOpen);
	}
	
	@Test
	public void go_up_works_when_desired_floor_above_current_level_and_below_number_of_levels_and_door_not_open() {
		ourElevator.GoUp(5);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(5, currentLevel);
	}
	
	@Test 
	public void go_up_doesnt_work_when_desired_floor_below_current_level() {
		ourElevator.GoUp(0);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(1, currentLevel);
	}
	
	@Test
	public void go_up_doesnt_work_when_desired_floor_is_greter_than_number_of_levels() {
		ourElevator.GoUp(22);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(1, currentLevel);
	}
	
	@Test
	public void go_up_doesnt_work_when_the_door_is_open() {
		ourElevator.OpenDoor();
		ourElevator.GoUp(15);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(1, currentLevel);
	}
	
	@Test
	public void go_down_works_when_door_closed_desired_floor_is_below_current_level_and_desired_floor_is_positive() {
		ourElevator.GoUp(15);
		ourElevator.GoDown(10);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(10, currentLevel);
	}
	
	@Test
	public void go_down_doesnt_work_when_door_is_open() {
		ourElevator.GoUp(15);
		ourElevator.OpenDoor();
		ourElevator.GoDown(10);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(15, currentLevel);
	}
	
	@Test
	public void go_down_doesnt_work_when_desired_floor_is_greater_than_current_level() {
		ourElevator.GoUp(15);
		ourElevator.GoDown(17);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(15, currentLevel);
	}
	
	@Test
	public void go_down_doesnt_work_when_floor_is_negative() {
		ourElevator.GoDown(-1);
		int currentLevel = ourElevator.getCurrentLevel();
		Assert.assertEquals(1, currentLevel);
	}
	

	
}
