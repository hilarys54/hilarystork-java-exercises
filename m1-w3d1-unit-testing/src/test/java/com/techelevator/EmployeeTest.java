package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeTest {
	private Employee ourEmployee;
	
	@Before
	public void setup() {
		ourEmployee = new Employee(1, "Hillary", "Clinton", "Government", 400000.00);
	}

	@Test
	public void initialization_values_are_stored_correctly() {
		int employeeId = ourEmployee.getEmployeeId();
		String firstName = ourEmployee.getFirstName();
		String lastName = ourEmployee.getLastName();
		String department = ourEmployee.getDepartment();
		double annualSalary = ourEmployee.getAnnualSalary();
		
		Assert.assertEquals(1, employeeId);
		Assert.assertEquals("Hillary", firstName);
		Assert.assertEquals("Clinton", lastName);
		Assert.assertEquals("Government", department);
		Assert.assertEquals(400000.00, annualSalary, 0);
	}
	
	@Test 
	public void get_full_name_works_correctly() {
		String fullName = ourEmployee.getFullName();
		Assert.assertEquals("Clinton, Hillary", fullName);
	}
	
	@Test
	public void raise_salary_works_correctly() {
		ourEmployee.RaiseSalary(50.00);
		double annualSalary = ourEmployee.getAnnualSalary();
		Assert.assertEquals(600000.00, annualSalary, 0);
	}

}
