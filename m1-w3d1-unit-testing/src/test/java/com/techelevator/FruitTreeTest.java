package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FruitTreeTest {
	private FruitTree ourFruitTree;
	
	@Before
	public void setup() {
		ourFruitTree = new FruitTree("pear", 100);	
	}
	
	@Test
	public void initialization_values_are_stored_correctly() {
		
		String typeOfFruit = ourFruitTree.getTypeOfFruit();
		int startingPiecesOfFruit = ourFruitTree.getPiecesOfFruitLeft();
		
		Assert.assertEquals("pear", typeOfFruit);
		Assert.assertEquals(100, startingPiecesOfFruit);
	}
	
	@Test
	public void pieces_of_fruit_left_is_stored_correctly() {
		
		int piecesOfFruitLeft = ourFruitTree.getPiecesOfFruitLeft();
		
		Assert.assertEquals(100, piecesOfFruitLeft);
	}
	
	@Test 
	public void are_able_to_pick_fruit() {
		
		boolean pickFruit = ourFruitTree.pickFruit(90);
		
		Assert.assertTrue("You tried to pick too much fruit.", pickFruit);
		Assert.assertEquals(10, ourFruitTree.getPiecesOfFruitLeft());
	}
	
	@Test
	public void not_enough_fruit_on_the_tree() {
		
		boolean pickFruit = ourFruitTree.pickFruit(200);
		
		Assert.assertFalse("You tried to pick too much fruit.", pickFruit);
		Assert.assertEquals(100, ourFruitTree.getPiecesOfFruitLeft());
	}
	
	@Test
	public void set_pick_fruit_works_correctly() {
		
		ourFruitTree.pickFruit(45);
		
		Assert.assertEquals(55, ourFruitTree.getPiecesOfFruitLeft());
	}

}
