package com.techelevator;

import  org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HomeworkAssignmentTest {
	private HomeworkAssignment ourHomeworkAssignment;

	@Before
	public void setup() {
		ourHomeworkAssignment = new HomeworkAssignment(50);
	}
	
	@Test
	public void initialization_values_are_stored_correctly() {
		
		int possibleMarks = ourHomeworkAssignment.getPossibleMarks();
		
		Assert.assertEquals(50, possibleMarks);
	}
	
	@Test
	public void total_marks_is_is_stored_correctly() {
		
		int totalMarks = ourHomeworkAssignment.getTotalMarks();
		
		Assert.assertEquals(0, totalMarks);
	}
	
	@Test
	public void submitter_name_is_stored_correctly() {
		
		String submitterName = ourHomeworkAssignment.getSubmitterName();
		
		Assert.assertSame(null, submitterName);
	}
	
	@Test
	public void an_A_is_stored_correctly() {
		
		ourHomeworkAssignment.setTotalMarks(46);
		
		Assert.assertSame("A", ourHomeworkAssignment.getLetterGrade());				
	}
	
	@Test
	public void a_B_is_stored_correctly() {
		
		ourHomeworkAssignment.setTotalMarks(43);
		
		Assert.assertSame("B", ourHomeworkAssignment.getLetterGrade());				
	}
	
	@Test
	public void a_C_is_stored_correctly() {
		
		ourHomeworkAssignment.setTotalMarks(38);
		
		Assert.assertSame("C", ourHomeworkAssignment.getLetterGrade());				
	}
	
	@Test
	public void a_D_is_stored_correctly() {
		
		ourHomeworkAssignment.setTotalMarks(32);
		
		Assert.assertSame("D", ourHomeworkAssignment.getLetterGrade());				
	}
	
	@Test
	public void an_F_is_stored_correctly() {
		
		ourHomeworkAssignment.setTotalMarks(25);
		
		Assert.assertSame("F", ourHomeworkAssignment.getLetterGrade());				
	}
	
	@Test
	public void submitter_is_stored_correctly() {
		
		ourHomeworkAssignment.setSubmitterName("Bill Clinton");
		
		Assert.assertSame("Bill Clinton", ourHomeworkAssignment.getSubmitterName());
	}
}
