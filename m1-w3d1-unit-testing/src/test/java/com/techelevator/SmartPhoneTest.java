package com.techelevator;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;


public class SmartPhoneTest {
	private SmartPhone ourSmartPhone;
	
	@Before 
	public void setup() {
		ourSmartPhone = new SmartPhone("614-440-1918", "AT&T", "IOS");
	}

	@Test
	public void initialization_values_are_stored_correctly() {
		String phoneNumber = ourSmartPhone.getPhoneNumber();
		String carrier = ourSmartPhone.getCarrier();
		String operatingSystem = ourSmartPhone.getOperatingSystem();
		
		Assert.assertEquals("614-440-1918", phoneNumber);
		Assert.assertEquals("AT&T", carrier);
		Assert.assertEquals("IOS", operatingSystem);
	}
	
	@Test
	public void operating_system_is_stored_correctly() {
		String operatingSystem = ourSmartPhone.getOperatingSystem();
		Assert.assertEquals("IOS", operatingSystem);
	}
	
	@Test
	public void battery_charge_is_stored_correctly() {
		int batteryCharge = ourSmartPhone.getBatteryCharge();
		Assert.assertEquals(100, batteryCharge);
	}
	
	@Test
	public void on_call_is_stored_correctly() {
		boolean onCall = ourSmartPhone.isOnCall();
		Assert.assertFalse(onCall);
	}
	
	@Test 
	public void answer_phone_is_stored_correctly() {
		ourSmartPhone.AnswerPhone();
		Assert.assertTrue(ourSmartPhone.isOnCall());
	}
	
	@Test
	public void hang_up_is_stored_correctly() {
		ourSmartPhone.HangUp();
		Assert.assertFalse(ourSmartPhone.isOnCall());
	}
	
	@Test
	public void recharge_battery_is_stored_correctly() {
		ourSmartPhone.RechargeBattery();
		Assert.assertEquals(100, ourSmartPhone.getBatteryCharge());
	}
	
	@Test
	public void call_is_stored_correctly() {
		ourSmartPhone.Call("614-792-1918", 20);
		int batteryCharge = ourSmartPhone.getBatteryCharge();
		
		Assert.assertTrue(ourSmartPhone.isOnCall());
		Assert.assertEquals(80, batteryCharge);
	}
	

}
