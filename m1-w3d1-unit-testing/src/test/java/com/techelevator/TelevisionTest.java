package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TelevisionTest {
	private Television television;
	
	@Before
	public void setup() {
		television = new Television();
	}
	

	@Test
	public void is_on_is_stored_correctly() {
		
		boolean isOn = television.isOn();
		
		Assert.assertFalse(isOn);	
	}
	
	@Test
	public void current_volume_is_stored_correctly() {
		
		int currentVolume = television.getCurrentVolume();
		
		Assert.assertEquals(0, currentVolume);
	}
	
	@Test
	public void selected_channel_is_stored_correctly() {
		
		int selectedChannel = television.getSelectedChannel();
		
		Assert.assertEquals(3, selectedChannel);
	}
	
	@Test
	public void turn_on_is_stored_correctly() {
		
		television.TurnOn();
		
		Assert.assertTrue(television.isOn());	
	}
	
	@Test
	public void turn_off_is_stored_correctly() {
		
		television.TurnOff();
		
		Assert.assertFalse(television.isOn());	
	}
	
	@Test
	public void raise_volume_is_stored_correctly() {
		
		television.RaiseVolume();
		
		Assert.assertEquals(1, television.getCurrentVolume());
	}
	
	@Test
	public void lower_volume_is_stored_correctly() {
		
		television.LowerVolume();
		
		Assert.assertEquals(-1, television.getCurrentVolume());
	}
	
	@Test
	public void change_channel_works_when_tvon_channel_between_3_18() {
		
		television.TurnOn();
		television.ChangeChannel(10);
		
		Assert.assertEquals(10, television.getSelectedChannel());
	}
	
	@Test
	public void change_channel_works_when_tvon_channel_isnt_between_3_18() {
		
		television.TurnOn();
		television.ChangeChannel(2);
		
		Assert.assertEquals(3, television.getSelectedChannel());
	}
	
	@Test
	public void change_channel_works_when_tvoff_channel_between_3_18() {
		
		television.TurnOff();
		television.ChangeChannel(16);
		
		Assert.assertEquals(3, television.getSelectedChannel());
	}
	
	@Test
	public void change_channel_works_when_tvoff_channel_isnt_between_3_18() {
		
		television.TurnOff();
		television.ChangeChannel(2);
		
		Assert.assertEquals(3, television.getSelectedChannel());
	}
}


