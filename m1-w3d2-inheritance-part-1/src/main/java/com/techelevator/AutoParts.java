package com.techelevator;

public class AutoParts {
	
	private String name;
	private String manufacturer;
	private int partNumber;
	private double weight;
	private double price;
	
	public AutoParts(String name, String manufacturer, int partNumber, double weight, double price) {
		this.name = name;
		this.manufacturer = manufacturer;
		this.partNumber = partNumber;
		this.weight = weight;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public Integer getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(Integer partNumber) {
		this.partNumber = partNumber;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

}
