package com.techelevator;

public class DoorHandle extends AutoParts {
	
	private boolean isTouchlessHandle;
	private String materialType;
	
	public DoorHandle(String name, String manufacturer, int partNumber, double weight, double price) {
		super(name, manufacturer, partNumber, weight, price);
		this.isTouchlessHandle = isTouchlessHandle;
		this.materialType = materialType;
	}
	
	public boolean isTouchlessHandle() {
		return isTouchlessHandle;
	}
	public void setTouchlessHandle(boolean isTouchlessHandle) {
		this.isTouchlessHandle = isTouchlessHandle;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	
	
	

}
