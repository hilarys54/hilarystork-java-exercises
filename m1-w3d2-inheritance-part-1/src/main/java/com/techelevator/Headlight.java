package com.techelevator;

public class Headlight extends AutoParts {
	
	private int lumens;
	private int lifeInHours;
	
	public Headlight(String name, String manufacturer, int partNumber, double weight, double price) {
		super(name, manufacturer, partNumber, weight, price);
		this.lumens = lumens;
		this.lifeInHours = lifeInHours;
	}
	
	public int getLumens() {
		return lumens;
	}
	public void setLumens(int lumens) {
		this.lumens = lumens;
	}
	public int getLifeInHours() {
		return lifeInHours;
	}
	public void setLifeInHours(int lifeInHours) {
		this.lifeInHours = lifeInHours;
	}
	
	

}
