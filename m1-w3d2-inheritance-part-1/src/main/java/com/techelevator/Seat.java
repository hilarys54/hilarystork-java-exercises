package com.techelevator;

public class Seat extends AutoParts {
	
	private boolean includesHeater;
	private String materialType;
	
	public Seat(String name, String manufacturer, int partNumber, double weight, double price) {
		super(name, manufacturer, partNumber, weight, price);
		this.includesHeater = includesHeater;
		this.materialType = materialType;
	}
	
	public boolean isIncludesHeater() {
		return includesHeater;
	}
	public void setIncludesHeater(boolean includesHeater) {
		this.includesHeater = includesHeater;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	
	

}
