package com.techelevator;

public class Stereo extends AutoParts {
	
	private boolean hasAuxInput;
	private int heightInInches;
	private int numberOfStationPresets;
	
	public Stereo(String name, String manufacturer, int partNumber, double weight, double price) {
		super(name, manufacturer, partNumber, weight, price);
		this.hasAuxInput = hasAuxInput;
		this.heightInInches = heightInInches;
		this.numberOfStationPresets = numberOfStationPresets;
	}
	
	public boolean isHasAuxInput() {
		return hasAuxInput;
	}
	public void setHasAuxInput(boolean hasAuxInput) {
		this.hasAuxInput = hasAuxInput;
	}
	public int getHeightInInches() {
		return heightInInches;
	}
	public void setHeightInInches(int heightInInches) {
		this.heightInInches = heightInInches;
	}
	public int getNumberOfStationPresets() {
		return numberOfStationPresets;
	}
	public void setNumberOfStationPresets(int numberOfStationPresets) {
		this.numberOfStationPresets = numberOfStationPresets;
	}
	
	
	

}
