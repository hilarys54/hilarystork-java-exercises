package com.techelevator;

public class Tire extends AutoParts {
	
	private int speedRating;
	private int recommendedPSI;
	private boolean handlesSnowWell;
	
	public Tire(String name, String manufacturer, int partNumber, double weight, double price) {
		super(name, manufacturer, partNumber, weight, price);
		this.speedRating = speedRating;
		this.recommendedPSI = recommendedPSI;
		this.handlesSnowWell = handlesSnowWell;
	}
	
	public int getSpeedRating() {
		return speedRating;
	}
	public void setSpeedRating(int speedRating) {
		this.speedRating = speedRating;
	}
	public int getRecommendedPSI() {
		return recommendedPSI;
	}
	public void setRecommendedPSI(int recommendedPSI) {
		this.recommendedPSI = recommendedPSI;
	}
	public boolean isHandlesSnowWell() {
		return handlesSnowWell;
	}
	public void setHandlesSnowWell(boolean handlesSnowWell) {
		this.handlesSnowWell = handlesSnowWell;
	}
	
	
	

}
