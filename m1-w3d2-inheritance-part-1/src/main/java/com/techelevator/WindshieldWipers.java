package com.techelevator;

public class WindshieldWipers extends AutoParts {
	
	private int length;
	private boolean isDriverSide;
	
	public WindshieldWipers(String name, String manufacturer, int partNumber, double weight, double price) {
		super(name, manufacturer, partNumber, weight, price);
		this.length = length;
		this.isDriverSide = isDriverSide;
	}
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean isDriverSide() {
		return isDriverSide;
	}
	public void setDriverSide(boolean isDriverSide) {
		this.isDriverSide = isDriverSide;
	}
	
	

}
