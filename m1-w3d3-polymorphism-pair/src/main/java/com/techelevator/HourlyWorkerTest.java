package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HourlyWorkerTest {
	private HourlyWorker newHourlyWorker;
	
	@Before
	public void setup() {
		newHourlyWorker = new HourlyWorker("Donald", "Trump", 20.00);
	}


	@Test
	public void initialization_values_are_stored_correctly() {
		String firstName = newHourlyWorker.getFirstName();
		String lastName = newHourlyWorker.getLastName();
		double hourlyRate = newHourlyWorker.getHourlyRate();
		Assert.assertEquals("Donald", firstName);
		Assert.assertEquals("Trump", lastName);
		Assert.assertEquals(20.00, hourlyRate, 0.01);
	
	}
	
	@Test
	public void hours_worked_initialization() {
		int hoursWorked = newHourlyWorker.getHoursWorked();
		Assert.assertEquals(newHourlyWorker.getHoursWorked(), hoursWorked);	
	}

}
