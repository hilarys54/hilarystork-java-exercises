package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VolunteerTest {
	private Volunteer newVolunteer;

	@Before
	public void setup() {
		newVolunteer = new Volunteer("Michael", "Jackson");
	}
		
	@Test
	public void initialization_values_are_stored_correctly() {
		String firstName = newVolunteer.getFirstName();
		String lastName = newVolunteer.getLastName();
		Assert.assertEquals("Michael", firstName);
		Assert.assertEquals("Jackson", lastName);
	
	}
	
	@Test
	public void hours_worked_initialization() {
		int hoursWorked = newVolunteer.getHoursWorked();
		Assert.assertEquals(newVolunteer.getHoursWorked(), hoursWorked);	
	}
	@Test
	public void weekly_pay_initialization() {
		int calculateWeeklyPay = (int)newVolunteer.getCalculateWeeklyPay();
		Assert.assertEquals((int)newVolunteer.getCalculateWeeklyPay(), calculateWeeklyPay);
	}
}
