package com.techelevator;

public interface DeliveryDriver {	

	public int getDistance();
	public int getWeight();
	public double getCalculateRate();
}
