package com.techelevator;

import com.techelevator.DeliveryDriver;
import com.techelevator.FexEd;
import com.techelevator.PostalService;
import com.techelevator.SPU;

public class Driver {
	
	public int distance;
	public int weight;
	
	public Driver (int distance, int weight) {
		this.distance = distance;
		this.weight = weight;
	}
	
	public static void main(String[] args) {
		
		DeliveryDriver[] ourDeliveryDriver = new DeliveryDriver[] { new FexEd(), new PostalService(), new SPU() };
		
		for(DeliveryDriver deliveryDriver : ourDeliveryDriver) {
			int distance = deliveryDriver.getDistance();
			int weight = deliveryDriver.getWeight();
			double calculateRate = deliveryDriver.getCalculateRate();
			System.out.println("Please enter the weight of the package?");
			System.out.println("(P)ounds or (O)unces?");
			System.out.println("What distance will it be traveling to?");
			System.out.println("Delivery Method" + "\t\t\t" + "$ cost");
			System.out.println("Postal Service (1st Class) " + calculateRate);
			System.out.println("Postal Service (2nd Class) " + calculateRate );
			System.out.println("Postal Service (3rd Class) " + calculateRate);
			System.out.println("FexEd " + calculateRate);
			System.out.println("SPU (4-day ground) " + calculateRate);
			System.out.println("SPU (2-day business) " + calculateRate);
			System.out.println("SPU (next-day) " + calculateRate);
		}
	}
}
