package com.techelevator;

public class PostalService implements DeliveryDriver {

	public int distance;
	public int weight;
	
	public String getClass;
	
	public PostalService (int distance, int weight, String getClass) {
		this.distance = distance;
		this.weight = weight;
		this.getClass = getClass;
	}
	
	@Override
	public int getDistance() {
		return distance;
	}

	@Override
	public int getWeight() {
		return weight;
	}
	
	@Override
	public int rate; 
	if (getClass == "first") {
		if (weight > 0 && weight <= 2) {
			rate = 0.035;
		} else if (weight >= 3 && weight <= 8) {
			rate = 0.040;
		} else if (weight >= 9 && weight <= 15) {
			rate = 0.047;
		} else if (weight >= 16 && weight <= 48) {
			rate = 0.195;
		} else if (weight >= 64 && weight <= 128) {
			rate = 0.450;
		} else {
			rate = 0.500;
		}
	} else if (getClass == "second") {
		if (weight > 0 && weight <= 2) {
			rate = 0.0035;
		} else if (weight >= 3 && weight <= 8) {
			rate = 0.0040;
		} else if (weight >= 9 && weight <= 15) {
			rate = 0.0047;
		} else if (weight >= 16 && weight <= 48) {
			rate = 0.0195;
		} else if (weight >= 64 && weight <= 128) {
			rate = 0.0450;
		} else {
			rate = 0.0500;
		}
	} else {
		if (weight > 0 && weight <= 2) {
			rate = 0.0020;
		} else if (weight >= 3 && weight <= 8) {
			rate = 0.0022;
		} else if (weight >= 9 && weight <= 15) {
			rate = 0.0024;
		} else if (weight >= 16 && weight <= 48) {
			rate = 0.0150;
		} else if (weight >= 64 && weight <= 128) {
			rate = 0.0160;
		} else {
			rate = 0.0170;
		}
		
	}
	
	@Override
	public double getCalculateRate() {
		return distance * rate;
	}
	

}
