package com.techelevator;

public class CheckingAccount extends BankAccount {

	public CheckingAccount(String accountHolderName, String accountNumber) {
		super(accountHolderName, accountNumber);
	}
	
	@Override
	public DollarAmount withdraw(DollarAmount amountToWithdraw) {
		if (balance.plus(new DollarAmount(10000)).isLessThan(amountToWithdraw)) {
			amountToWithdraw.equals(0);
			System.out.println("You cannot have a balance less than -$100.");
			
		} else if (balance.isLessThan(new DollarAmount(0))) {
			balance = balance.minus(amountToWithdraw);
			balance = balance.minus(new DollarAmount(1000));
		} else {
			balance = balance.minus(amountToWithdraw);	
		}
		return balance;
	}
}

	
	