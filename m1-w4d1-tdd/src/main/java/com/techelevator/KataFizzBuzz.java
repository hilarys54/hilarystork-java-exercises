package com.techelevator;

public class KataFizzBuzz {
	public String fizzBuzz(int nums) {
		if (nums % 3 == 0 && nums % 5 == 0) {
			return "FizzBuzz";
		} else if (nums % 3 == 0 || String.valueOf(nums).contains("3")) {
			return "Fizz";
		} else if (nums % 5 == 0 || String.valueOf(nums).contains("5")) {
			return "Buzz";
		} else {
			return Integer.toString(nums);
		}
	}
}

