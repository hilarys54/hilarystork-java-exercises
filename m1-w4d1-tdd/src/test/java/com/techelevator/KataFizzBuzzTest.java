package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest {
	/*1 ->  returns "1"
2 -> returns "2"
3 -> returns "Fizz"
4 -> returns "4"
5 -> returns "Buzz" 
15 -> returns "FizzBuzz"
... etc up to 100*/
	
	@Test
	public void given_1_return_one() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(1);
		
		//Assert
		Assert.assertEquals("1", results);
	}
	@Test
	public void given_2_return_two() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(2);
		
		//Assert
		Assert.assertEquals("2", results);
	}
	@Test
	public void given_3_return_fizz() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(3);
		
		//Assert
		Assert.assertEquals("Fizz", results);
	}
	@Test
	public void given_4_return_four() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(4);
		
		//Assert
		Assert.assertEquals("4", results);
	}
	@Test
	public void given_5_return_buzz() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(5);
		
		//Assert
		Assert.assertEquals("Buzz", results);
	}
	@Test
	public void given_15_return_fizzbuzz() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(15);
		
		//Assert
		Assert.assertEquals("FizzBuzz", results);
	}
	@Test
	public void given_103_return_one_hundred_and_one() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(101);
		
		//Assert
		Assert.assertEquals("101", results);
	}
	@Test
	public void given_103_return_one_hundred_and_three() {
		//Arrange
		KataFizzBuzz nums = new KataFizzBuzz();
	
		//Act
		String results = nums.fizzBuzz(103);
		
		//Assert
		Assert.assertEquals("Fizz", results);
	}
	
}
