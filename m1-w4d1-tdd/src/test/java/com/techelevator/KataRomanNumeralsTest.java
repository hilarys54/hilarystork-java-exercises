package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataRomanNumeralsTest {
//	1 ---> I
//	10 --> X
//	7 ---> VII
	
	@Test
	public void given_1_return_I() {
	
		//Arrange
		KataRomanNumerals nums = new KataRomanNumerals();
		
		//Act
		String results = nums.numToRoman(1);
		
		//Assert
		Assert.assertEquals("I", results);
	}
	
	@Test
	public void given_10_return_X() {
	
		//Arrange
		KataRomanNumerals num = new KataRomanNumerals();
		
		//Act
		String results = num.numToRoman(10);
		
		//Assert
		Assert.assertEquals("X", results);
	}
	@Test
	public void given_7_return_VII() {
	
		//Arrange
		KataRomanNumerals num = new KataRomanNumerals();
		
		//Act
		String results = num.numToRoman(7);
		
		//Assert
		Assert.assertEquals("VII", results);
	}
	
	@Test
	public void given_2008_return_MMVIII() {
	
		//Arrange
		KataRomanNumerals num = new KataRomanNumerals();
		
		//Act
		String results = num.numToRoman(2008);
		
		//Assert
		Assert.assertEquals("MMVIII", results);
	}
	
	@Test
	public void given_4000_return_invalid() {
	
		//Arrange
		KataRomanNumerals num = new KataRomanNumerals();
		
		//Act
		String results = num.numToRoman(4000);
		
		//Assert
		Assert.assertEquals("Invalid roman numeral value!", results);
	}
	
	@Test
	public void given_negative_1_return_invalid() {
	
		//Arrange
		KataRomanNumerals num = new KataRomanNumerals();
		
		//Act
		String results = num.numToRoman(-1);
		
		//Assert
		Assert.assertEquals("Invalid roman numeral value!", results);
	}
}
