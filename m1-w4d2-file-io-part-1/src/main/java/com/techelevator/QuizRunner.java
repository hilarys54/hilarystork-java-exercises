package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuizRunner {
	public QuizRunner(String fileName) {
		BufferedReader reader = null;
		Scanner scanner = null;
		
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			String filePath = classLoader.getResource(fileName).getFile();
			File quizFile = new File(filePath);
			FileReader fileReader = new FileReader(quizFile);
			
			char[] buffer = new char[1024];
			int charRead = fileReader.read(buffer);
			List<String> stringList = new ArrayList<String>();
			char[] result = new char[1024];
			
			while(charRead > 0) {
				int lastNewLine = 0;
				for(int i = 0; i < charRead; i++) {
					if(buffer[i] == '\n') {
						stringList.add(new String(result));
						result = new char[1024];
						lastNewLine = i;
					} else {
						result[i - lastNewLine] = buffer[i];
					}
				}
				stringList.add(new String(result));
				
				charRead = fileReader.read(buffer);
			}
			
			
			
			reader = new BufferedReader(fileReader);
			scanner = new Scanner(System.in);
			
			int totalCorrect = 0;
			String line = reader.readLine();	
			while(line != null) {
				int correctAnswer = askQuestion(line);
				boolean wasCorrect = getUserChoice(scanner, correctAnswer);
				if(wasCorrect) {
					totalCorrect++;
				}
				line = reader.readLine();
			}
			
			System.out.println("You got " + totalCorrect + " answer(s) right!");
			
		} catch (FileNotFoundException notFound) {
			System.err.println("File was not found!");
			notFound.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} if (scanner != null) {
				scanner.close();
				}
			}
		}
	/**
	 * Asks the user a single question and returns their choice
	 * @param question A pipe delimited string containing the 
	 * 		question and the possible choices.  The correct choice is marked by an *.
	 * @return The correct answer index.
	 */
	private int askQuestion(String question) {
		String[] parts = question.split("\\|");
		System.out.println(parts[0]);
		
		int correctAnswer = 0;
		
		for(int i = 1; i < parts.length; i++) {
			String answer = parts[i];
			
			if (answer.endsWith("*")) {
				correctAnswer = i;
				answer = answer.replace("*", "");
			} 
			
			System.out.println(i + ")" + answer);
		}
		return correctAnswer;
	}
	
	/**
	 * Prompt user for their choice and return whether it is correct.
	 * @param scanner Scanner to read user input.
	 * @param correctAnswer Index of the correct answer.
	 * @return True if the user chose the correct answer, otherwise false.
	 */
	private boolean getUserChoice(Scanner scanner, int correctAnswer) {
		int userChoice = scanner.nextInt();

		
		if (userChoice == correctAnswer) {
			System.out.println("You got it! You rock!");
			return true;
		} else {
			System.out.println("Wrong Dummy!");
			return false;
		}
	}
}
