-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
--    Rows: 30
SELECT title
FROM film
WHERE film_id IN (
	SELECT film_id
	FROM film_actor
	WHERE actor_id = (
		SELECT actor_id
		FROM actor
		WHERE first_name = 'NICK' AND last_name = 'STALLONE'));

SELECT title
FROM film
INNER JOIN film_actor fa
ON film.film_id= fa.film_id
INNER JOIN actor
ON fa.actor_id = actor.actor_id
WHERE actor.first_name = 'NICK' AND last_name = 'STALLONE';


		



-- 2. All of the films that Rita Reynolds has appeared in
--    Rows: 20

SELECT title
FROM film
WHERE film_id IN (
	SELECT film_id
	FROM film_actor
	WHERE actor_id = (
		SELECT actor_id
		FROM actor
		WHERE first_name = 'RITA' AND last_name = 'REYNOLDS'));


-- 3. All of the films that Judy Dean or River Dean have appeared in
--    Rows: 46

SELECT title
FROM film
INNER JOIN film_actor fa
ON film.film_id= fa.film_id
INNER JOIN actor
ON fa.actor_id = actor.actor_id
WHERE (actor.first_name = 'JUDY' AND last_name = 'DEAN') OR (actor.first_name = 'RIVER' AND last_name = 'DEAN');

-- 4. All of the the ‘Documentary’ films
--    Rows: 68
SELECT title
FROM film
WHERE film_id IN (
	SELECT film_id 
	FROM film_category 
	WHERE category_id = (
		SELECT category_id 
		FROM category 
		WHERE name = 'Documentary'));

-- 5. All of the ‘Comedy’ films
--    Rows: 58

SELECT title
FROM film
WHERE film_id IN (
	SELECT film_id 
	FROM film_category 
	WHERE category_id = (
		SELECT category_id 
		FROM category 
		WHERE name = 'Comedy'));

-- 6. All of the ‘Children’ films that are rated ‘G’
--    Rows: 10 
SELECT title
FROM film
WHERE rating = 'G' AND film_id IN (
	SELECT film_id 
	FROM film_category 
	WHERE category_id = (
		SELECT category_id 
		FROM category 
		WHERE name = 'Children'));



-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
--    Rows: 3

SELECT title
FROM film
WHERE rating = 'G' AND length < 120 AND film_id IN (
	SELECT film_id 
	FROM film_category 
	WHERE category_id = (
		SELECT category_id 
		FROM category 
		WHERE name = 'Family'));

-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
--    Rows: 9

SELECT title
FROM film
WHERE rating = 'G' AND film_id IN (
	SELECT film_id 
	FROM film_actor 
	WHERE actor_id = (
		SELECT actor_id 
		FROM actor 
		WHERE first_name = 'MATTHEW' AND last_name = 'LEIGH'));

-- 9. All of the ‘Sci-Fi’ films released in 2006
--    Rows: 61

SELECT title
FROM film 
WHERE release_year = 2006 and film_id IN (
	SELECT film_id 
	FROM film_category 
	WHERE category_id = (
		SELECT category_id 
		FROM category 
		WHERE name = 'Sci-Fi'));


-- 10. All of the ‘Action’ films starring Nick Stallone
--     Rows: 2

SELECT title
FROM film
INNER JOIN film_category
ON film.film_id = film_category.film_id
INNER JOIN category
ON film_category.category_id = category.category_id
INNER JOIN film_actor
ON film.film_id= film_actor.film_id
INNER JOIN actor
ON film_actor.actor_id = actor.actor_id
WHERE actor.first_name = 'NICK' AND actor.last_name = 'STALLONE' AND category.name = 'Action';



-- 11. The address of all stores, including street address, city, district, and country
--     Rows: 2

SELECT address, address2, city.city, district, country.country
FROM address
INNER JOIN city ON address.city_id = city.city_id
INNER JOIN store ON address.address_id = store.address_id
INNER JOIN country ON city.country_id = country.country_id
WHERE store.store_id IS NOT NULL;

-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
--     Rows: 2

SELECT store.store_id, address.address, (staff.first_name|| ', ' ||staff.last_name)
FROM store
INNER JOIN address ON store.address_id = address.address_id
INNER JOIN staff ON store.manager_staff_id = staff.staff_id;

-- 13. The first and last name of the top ten customers ranked by number of rentals 
--     Hint: #1 should be “ELEANOR HUNT” with 46 rentals, #10 should have 39 rentals

SELECT customer.first_name, customer.last_name, COUNT(rental.customer_id) as count
FROM customer
INNER JOIN rental ON customer.customer_id = rental.customer_id
GROUP BY (customer.customer_id)
ORDER BY count DESC
LIMIT 10;


-- 14. The first and last name of the top ten customers ranked by dollars spent 
--     Hint: #1 should be “KARL SEAL” with 221.55 spent, #10 should be “ANA BRADLEY” with 174.66 spent

SELECT customer.first_name, customer.last_name, SUM(payment.amount) as sum
FROM customer
INNER JOIN rental ON customer.customer_id = rental.customer_id
INNER JOIN payment ON rental.rental_id = payment.rental_id
GROUP BY (customer.customer_id)
ORDER BY sum DESC
LIMIT 10;

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store 
--     Hint: Store 1 has 7928 total rentals and Store 2 has 8121 total rentals

SELECT store.store_id, address.address, COUNT(payment_id), SUM(amount), ROUND(AVG(amount), 2)
FROM store
INNER JOIN address ON store.address_id = address.address_id
INNER JOIN customer ON customer.store_id = store.store_id
INNER JOIN payment ON customer.customer_id = payment.customer_id
GROUP BY store.store_id, address.address;


-- 16. The top ten film titles by number of rentals 
--     Hint: #1 should be “BUCKET BROTHERHOOD” with 34 rentals and #10 should have 31 rentals

SELECT title, COUNT(rental.inventory_id) as count
FROM film
INNER JOIN inventory ON film.film_id = inventory.film_id
INNER JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY title
ORDER BY count DESC
LIMIT 10;


-- 17. The top five film categories by number of rentals 
--     Hint: #1 should be “Sports” with 1179 rentals and #5 should be “Family” with 1096 rentals

SELECT category.name, COUNT(rental.inventory_id) as count
FROM film
INNER JOIN inventory ON film.film_id = inventory.film_id
INNER JOIN rental ON inventory.inventory_id = rental.inventory_id
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id
GROUP BY name
ORDER BY count DESC
LIMIT 10;


-- 18. The top five Action film titles by number of rentals 
--     Hint: #1 should have 30 rentals and #5 should have 28 rentals

SELECT title, COUNT(rental.inventory_id) as count
FROM film
INNER JOIN inventory ON film.film_id = inventory.film_id
INNER JOIN rental ON inventory.inventory_id = rental.inventory_id
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id
WHERE category.name = 'Action'
GROUP BY title
ORDER BY count DESC
LIMIT 10;


-- 19. The top 10 actors ranked by number of rentals of films starring that actor 
--     Hint: #1 should be “GINA DEGENERES” with 753 rentals and #10 should be “SEAN GUINESS” with 599 rentals

SELECT actor.actor_id, (actor.first_name || ' ' || actor.last_name), COUNT(actor.actor_id) as count
FROM film
INNER JOIN inventory ON film.film_id = inventory.film_id
INNER JOIN rental ON inventory.inventory_id = rental.inventory_id
INNER JOIN film_actor ON film.film_id = film_actor.film_id
INNER JOIN actor ON film_actor.actor_id = actor.actor_id
GROUP BY actor.actor_id
ORDER BY count DESC
LIMIT 10;

-- 20. The top 5 “Comedy” actors ranked by number of rentals of films in the “Comedy” category starring that actor 
--     Hint: #1 should have 87 rentals and #10 should have 72 rentals

SELECT actor.actor_id, (actor.first_name || ' ' || actor.last_name), COUNT(category.category_id) as count
FROM film
INNER JOIN inventory ON film.film_id = inventory.film_id
INNER JOIN rental ON inventory.inventory_id = rental.inventory_id
INNER JOIN film_actor ON film.film_id = film_actor.film_id
INNER JOIN actor ON film_actor.actor_id = actor.actor_id
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id
WHERE category.name = 'Comedy'
GROUP BY actor.actor_id
ORDER BY count DESC
LIMIT 5;




