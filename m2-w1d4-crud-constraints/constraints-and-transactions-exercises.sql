-- Write queries to return the following:
-- The following changes are applied to the "pagila" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.
INSERT INTO actor (first_name, last_name) VALUES ('HAMPTON', 'AVENUE');
INSERT INTO actor (first_name, last_name) VALUES ('LISA', 'BYWAY');


-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in 
-- ancient Greece", to the film table. The movie was released in 2008 in English. 
-- Since its an epic, the run length is 3hrs and 18mins. There are no special 
-- features, the film speaks for itself, and doesn't need any gimmicks.	

INSERT INTO film (title, description, release_year, language_id, length) 
VALUES ('Euclidean PI', 'The epic story of Euclid as a pizza delivery boy in ancient Greece', 2008, (SELECT language_id FROM language WHERE name = 'English'), 198);

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly 
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.

INSERT INTO film_actor (film_id, actor_id)
VALUES ((SELECT film_id FROM film WHERE title = 'Euclidean PI'), (SELECT actor_id FROM actor WHERE first_name = 'LISA' AND last_name = 'BYWAY'));

-- 4. Add Mathmagical to the category table.

INSERT INTO category (name)
VALUES ('Mathmagical');

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI", 
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"

INSERT INTO film_category (film_id, category_id) 
VALUES ((SELECT film_id FROM film WHERE title IN ('Euclidean PI')), (SELECT category_id FROM category WHERE name = 'Mathmagical'));

INSERT INTO film_category (film_id, category_id) 
VALUES ((SELECT film_id FROM film WHERE title IN ('EGG IGBY')), (SELECT category_id FROM category WHERE name = 'Mathmagical'));

INSERT INTO film_category (film_id, category_id) 
VALUES ((SELECT film_id FROM film WHERE title IN ('KARATE MOON')), (SELECT category_id FROM category WHERE name = 'Mathmagical'));

INSERT INTO film_category (film_id, category_id) 
VALUES ((SELECT film_id FROM film WHERE title IN ('RANDOM GO')), (SELECT category_id FROM category WHERE name = 'Mathmagical'));

INSERT INTO film_category (film_id, category_id) 
VALUES ((SELECT film_id FROM film WHERE title IN ('YOUNG LANGUAGE')), (SELECT category_id FROM category WHERE name = 'Mathmagical'));

-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films 
-- accordingly.
-- (5 rows affected)

UPDATE film
SET rating = 'G'
WHERE film_id IN (
	SELECT film_id FROM film_category WHERE category_id IN (
		SELECT category_id FROM category WHERE name = 'Mathmagical'));

-- 7. Add a copy of "Euclidean PI" to all the stores.

INSERT INTO inventory (film_id, store_id)
VALUES ((SELECT film_id FROM film WHERE title = 'Euclidean PI'), 1), ((SELECT film_id FROM film WHERE title = 'Euclidean PI'), 2);

-- 8. The Feds have stepped in and have impounded all copies of the pirated film, 
-- "Euclidean PI". The film has been seized from all stores, and needs to be 
-- deleted from the film table. Delete "Euclidean PI" from the film table. 
-- (Did it succeed? Why?)

DELETE FROM film
WHERE title = 'Euclidean PI';

-- It did not delete because the deletion violates foreign key constraint "film_actor_film_id_fkey" on table "film_actor".  The film_id Key associated with 'Euclidean PI' is referenced in the table film_actor.

-- 9. Delete Mathmagical from the category table. 
-- (Did it succeed? Why?)

DELETE FROM category 
WHERE name = 'Mathmagical';

--It did not delete because the deletion violates foreign key constraint "film_actor_film_id_fkey" on table "film_actor".  The category_id associated with 'Mathmagical' is referenced in the table film_category. 

-- 10. Delete all links to Mathmagical in the film_category tale. 
-- (Did it succeed? Why?)

DELETE FROM film_category
WHERE category_id = 17;

--It only worked when I had category_id = 17, but it didn't work when I had category_id IN (SELECT category_id FROM category WHERE name = 'Mathmagial')

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI". 
-- (Did either deletes succeed? Why?)

DELETE FROM category
WHERE name = 'Mathmagical';

DELETE FROM film
WHERE title = 'Euclidean PI';

--The first one worked but the second one did not.  The delete on table "film" violates foreign key constraint "film_actor_film_id_fkey" on table "film_actor".

-- 12. Check database metadata to determine all constraints of the film id, and 
-- describe any remaining adjustments needed before the film "Euclidean PI" can 
-- be removed from the film table.

--I would have to delete all references to the film_id associated with 'Euclidean PI' from the film_actor table to prevent any foreign key constraints.  Once those were deleted, I could then delete from film the row with the title 'Euclidean PI'.
