CREATE DATABASE Project_Organizer;

CREATE TABLE department
(
	department_number serial NOT NULL,
	name varchar(64) NOT NULL,
	number_employees integer integer NOT NULL,
	CONSTRAINT pk_department_number PRIMARY KEY (department_number)
);

CREATE TABLE project
(
	project_number serial NOT NULL,
	name varchar(64) NOT NULL,
	start_date date NOT NULL,
	number_employees integer NOT NULL,
	CONSTRAINT pk_project_number PRIMARY KEY (project_number)
);

CREATE TABLE employee
(
	employee_number serial NOT NULL,
	job_title varchar(64) NOT NULL,
	last_name varchar(64) NOT NULL,
	first_name varchar(64) NOT NULL,
	gender char(1) NOT NULL,
	birthday date NOT NULL,
	hire_date date NOT NULL,
	department_number integer NOT NULL,
	CONSTRAINT pk_employee_number PRIMARY KEY (employee_number)	
);

CREATE TABLE project_employee
(
	project_number integer NOT NULL,
	employee_number integer NOT NULL,
	CONSTRAINT pk_project_employee PRIMARY KEY (project_number, employee_number)
);


INSERT INTO department (name, number_employees) VALUES ('Human Resources', 27);
INSERT INTO department (name, number_employees) VALUES ('Sales', 40);
INSERT INTO department (name, number_employees) VALUES ('IT', 15);

INSERT INTO project (name, start_date, number_employees) VALUES ('The Best Project', 2000-08-16, 18);
INSERT INTO project (name, start_date, number_employees) VALUES ('The Worst Project', 2001-09-17, 20);
INSERT INTO project (name, start_date, number_employees) VALUES ('The First Project', 2002-10-18, 22);
INSERT INTO project (name, start_date, number_employees) VALUES ('The Last Project', 2003-11-19, 24);

INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES 	('Brain', 'Stork', 'Hilary', F, 1980-09-16, 2016-12-16, 1);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES 	('President', 'Clinton', 'Hillary', F, 1949-06-28, 2015-11-01, 2);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES ('Vice President', 'Kane', 'Tim', M, 1952-04-27, 2012-03-15, 3);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES 	('Former President', 'Obama', 'Barrack', M, 1955-02-03, 2010-01-08, 1);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES ('CEO', 'Lyden', 'Vicki', F, 1949-06-29, 2005-12-16, 2);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES 	('Boss', 'Dog', 'Snoop', F, 1961-09-24, 2002-10-10, 3);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES 	('Salesman', 'Trump', 'Donald', M, 1950-08-13, 2004-07-16, 1);
INSERT INTO employee (job_title, last_name, first_name, gender, birthday, hire_date, department_number) VALUES ('Head of HR', 'Kasich', 'John', M, 1955-06-02, 2005-11-05, 2);

INSERT INTO project_employee (project_number, employee_number) VALUES (1, 
	(SELECT employee_number FROM employee WHERE last_name = 'Stork' AND first_name = 'Hilary'));
INSERT INTO project_employee (project_number, employee_number) VALUES (2, 
	(SELECT employee_number FROM employee WHERE last_name = 'Clinton' AND first_name = 'Hillary'));
INSERT INTO project_employee (project_number, employee_number) VALUES (3, 
	(SELECT employee_number FROM employee WHERE last_name = 'Kane' AND first_name = 'Tim'));
INSERT INTO project_employee (project_number, employee_number) VALUES (4, 
	(SELECT employee_number FROM employee WHERE last_name = 'Obama' AND first_name = 'Barrack'));
	
ALTER TABLE project_employee
ADD FOREIGN KEY (project_number)
REFERENCES project(project_number);

ALTER TABLE project_employee
ADD FOREIGN KEY (employee_number)
REFERENCES employee(employee_number);

ALTER TABLE employee
ADD FOREIGN KEY (department_number)
REFERENCES department(department_number);

UPDATE TABLE department
SET number_employees = (
	SELECT COUNT(department_number)
	FROM employee
	WHERE employee.department_number = department.department_number
	GROUP BY department_number
);

UPDATE TABLE project
SET number_employees = (
	SELECT COUNT(project_number)
	FROM project_employee
	WHERE project_employee.project_number = project.project_number
	GROUP BY project_number
); 
