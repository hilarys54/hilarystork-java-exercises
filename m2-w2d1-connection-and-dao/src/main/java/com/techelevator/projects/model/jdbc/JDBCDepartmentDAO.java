package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;

public class JDBCDepartmentDAO implements DepartmentDAO {
	
	private JdbcTemplate jdbcTemplate;

	public JDBCDepartmentDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	
	@Override
	public List<Department> getAllDepartments() {
		String sqlGetDepartments;
		return new ArrayList<>();
	}

	@Override
	public List<Department> searchDepartmentsByName(String nameSearch) {
		ArrayList<Department> departments = new ArrayList<>();
		String sqlSearchDepartmentByName = "SELECT id, name" +
											"FROM department "+
											"WHERE name = ?";
		SqlRowSet results = jbdcTemplate.queryForRowSet(sqlSearchDepartmentByName, name);
		while(results.next()) {
			Department theDepartment = mapRowToDepartment(results);
			departments.add(theDepartment);
		}
		return departments;
	}

	@Override
	public void updateDepartmentName(Long departmentId, String departmentName) {
		
	}

	@Override
	public Department createDepartment(String departmentName) {
		return null;
	}

	@Override
	public Department getDepartmentById(Long id) {
		return null;
	}

}
