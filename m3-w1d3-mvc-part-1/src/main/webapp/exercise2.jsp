<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Exercise 2 - Fibonacci 25</title>
		<style>
			li {
				list-style-type: none;
			}
		</style>
	</head>
	<body>
		<h1>Exercise 2 - Fibonacci 25</h1>
		<ul>
		
		<c:set var="var1" value="0"/>
		<c:set var="var2" value="1"/>
		<c:set var="var3" value="${var1 +var2}"/>
			<%--
				Add a list item (i.e. <li>) for each of the first 25 numbers in the Fibonacci sequence
				
				See exercise2-fibonacci.png for example output
			 --%>
			<c:forEach begin="1" end="26" var="index">

				<li>${var3}</li>
				<c:set var="var3" value="${var1+var2}"/>
				<c:set var="var1" value="${var2}"/>
				<c:set var="var2" value="${var3}"/>
		
			</c:forEach>
		</ul>
	</body>
</html>