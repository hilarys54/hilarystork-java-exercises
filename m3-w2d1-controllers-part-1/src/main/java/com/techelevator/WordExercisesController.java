package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WordExercisesController {

	@RequestMapping("/lastTwoInput")
	public String handleLastTwoInput(HttpServletRequest request) {
		return "lastTwoInput";
	}
	
	@RequestMapping("/lastTwoResult")
	public String handleLastTwoResult(HttpServletRequest request) {
		List<String> original = new ArrayList<String>();
		List<String> flipped = new ArrayList<String>();
		
		for(int i = 1; i <= 10; i++) {
			String word = request.getParameter("word" + i);
			if(word != null) {
				String modified = null;
				if(word.length() >= 2) {
					modified = word.substring(0, word.length() - 2) + word.charAt(word.length() - 1) + word.charAt(word.length() - 2);
				} else if(word.length() == 1) {
					modified = word;
				}
				
				if(modified != null) {
					flipped.add(modified);
					original.add(word);
				}
			}
		}
		
		request.setAttribute("original", original);
		request.setAttribute("flipped", flipped);
		
		return "lastTwoResult";
	}
	
	@RequestMapping("/xOrderedNameInput")
	public String handleXOrderedNameInput(HttpServletRequest request) {
		return "xOrderedNameInput";
	}
	
	@RequestMapping("/xOrderedNameResult")
	public String handleXOrderedNameResult(HttpServletRequest request) {
		String firstName = request.getParameter("firstName");
		String middleInitial = request.getParameter("middleInitial");
		String lastName = request.getParameter("lastName");
		String order = request.getParameter("chooseOrder");
		String changed = new String();
				
		switch (order) {	
			case "1":
			changed = (firstName+ " " + middleInitial + " " + lastName);		
			break;
			case "2":
			changed = (firstName + " " + lastName);
			break;
			case "3":
			changed = (lastName + ", " + firstName + " " + middleInitial);
			break;
			case "4":
			changed = (lastName + ", " + firstName);
			break;
		}
		request.setAttribute("changed", changed);	
		return "xOrderedNameResult";
	}
	
	@RequestMapping("/colorizedNameInput")
	public String handleColorizedNameInput(HttpServletRequest request) {
		return "colorizedNameInput";
	}
	
	@RequestMapping("/colorizedNameResult")
	public String handleColorizedNameResult(HttpServletRequest request) {
		return "colorizedNameResult";
	}
	
	@RequestMapping("/fizzBuzzInput")
	public String handleFizzBuzzInput(HttpServletRequest request) {
		return "fizzBuzzInput";
	}
	
	@RequestMapping("/fizzBuzzResult")
	public String handleFizzBuzzResult(HttpServletRequest request) {
		return "fizzBuzzResult";
	}
}
