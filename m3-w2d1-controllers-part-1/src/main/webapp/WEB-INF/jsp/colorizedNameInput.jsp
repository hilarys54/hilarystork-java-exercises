<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Colorized Name Input</title>
</head>
<body>
<c:url var="formResult" value="/colorizedNameResult"/>
	<form action="${formResult}" method="GET">
	<h3>Enter Name</h3>
			<label for="firstName">First name:</label>
			<input type="text" name="firstName" id="firstName"/><br/>
			
			<label for="lastName">Last name:</label>
			<input type="text" name="lastName" id="lastName"/><br/>
			
			<h3><label for="chooseColor">Choose Color</label></h3>
			<br>
			<input type="checkbox" name="red" />Red
			<br>
			<input type="checkbox" name="blue" />Blue
			<br>
			<input type="checkbox" name="green" />Green
			<br>
			
			<input type="submit" value="Submit"/>
		</form>

</body>
</html>