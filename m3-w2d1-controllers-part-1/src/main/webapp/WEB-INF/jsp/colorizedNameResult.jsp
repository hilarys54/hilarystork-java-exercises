<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Colorized Name Result</title>
	</head>
	<body>
	<h1>Colorized Name</h1>
	<p>First name: ${param.firstName}
	<br>Last name: ${param.lastName}</p>
	
		<c:if test="${not empty param.red}">
			<p style="color:red">${param.firstName} ${param.lastName}</p>
		</c:if>
		<c:if test="${not empty param.blue}">
			<p style="color:blue">${param.firstName} ${param.lastName}</p>
		</c:if>
		<c:if test="${not empty param.green}">
			<p style="color:green">${param.firstName} ${param.lastName}</p>
		</c:if>
	</body>
</html>