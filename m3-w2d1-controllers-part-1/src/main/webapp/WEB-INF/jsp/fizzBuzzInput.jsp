<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Fizz Buzz Input</title>
</head>
<body>
<h3>Fizz Buzz Revisited</h3>
<c:url var="formResult" value="/fizzBuzzResult"/>
	<form action="${formResult}" method="GET">
	
		<label for="divisor1">Divisible By:</label>
		<input type="number" name="divisor1" id="divisor1"/><br/>
	
		<label for="divisor2">Divisible By:</label>
		<input type="number" name="divisor2" id="divisor2"/><br/>
		
		<label for="alternativeFizz">Alternative Fizz:</label>
		<input type="text" name="alternativeFizz" id="alternativeFizz"/><br/>
		
		<label for="alternativeBuzz">Alternative Buzz:</label>
		<input type="text" name="alternativeBuzz" id="alternativeBuzz"/><br/>
	
		<label for="ticket1">Ticket 1:</label>
		<input type="number" name="ticket1" id="ticket1"/><br/>
		
		<label for="ticket2">Ticket 2:</label>
		<input type="number" name="ticket2" id="ticket2"/><br/>
		
		<label for="ticket3">Ticket 3:</label>
		<input type="number" name="ticket3" id="ticket3"/><br/>
		
		<label for="ticket4">Ticket 4:</label>
		<input type="number" name="ticket4" id="ticket4"/><br/>
		
		<label for="ticket5">Ticket 5:</label>
		<input type="number" name="ticket5" id="ticket5"/><br/>
		
		<input type="submit" value="Submit"/>
		</form>

</body>
</html>