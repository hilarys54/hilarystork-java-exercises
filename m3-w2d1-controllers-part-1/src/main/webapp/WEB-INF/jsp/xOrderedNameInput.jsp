<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>

<title>Ordered Name</title>
</head>
<body>
	<c:url var="formResult" value="/xOrderedNameResult"/>
	<form action="${formResult}" method="GET">
	<h3>Enter Name</h3>
			<label for="firstName">First name:</label>
			<input type="text" name="firstName" id="firstName"/><br/>
			<label for="middleInitial">Middle initial:</label>
			<input type="text" name="middleInitial" id="middleInitial"/><br/>
			<label for="lastName">Last name:</label>
			<input type="text" name="lastName" id="lastName"/><br/>
			<h3><label for="chooseOrder">Choose Order</label></h3>
			<br>
			<input type="radio" name="chooseOrder" value="1">First MI Last
			<br>
			<input type="radio" name="chooseOrder" value="2" />First Last
			<br>
			<input type="radio" name="chooseOrder" value="3" />Last, First MI
			<br>
			<input type="radio" name="chooseOrder" value="4" />Last, First
			<br>
			<input type="submit" value="Submit"/>
		</form>


</body>
</html>