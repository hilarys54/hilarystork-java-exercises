package com.techelevator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;

@Controller 
public class HelloController {
	
	@Autowired
	ReviewDao reviewDao;

	@RequestMapping("/greeting")
	public String displayGreeting() {
		
		return "greeting";
	}
	
	@RequestMapping("/homePage")
	
	public String handleHomePage(HttpServletRequest request) {
			List<Review> reviewList =  new ArrayList<Review>();		
			reviewList = reviewDao.getAllReviews();
			request.setAttribute("reviewList", reviewList);	
		
		return "homePage";
	}
	
	@RequestMapping(path="/reviewInput", method=RequestMethod.GET)
	public String reviewInput(){

		return "reviewInput";
	}
	
	@RequestMapping(path="/reviewInput", method=RequestMethod.POST)
	public String processSpaceForumSubmission(@RequestParam String username,
											 @RequestParam int rating,
											 @RequestParam String title,
											 @RequestParam String text) {
		Review review = new Review();
		review.setUsername(username);
		review.setRating(rating);
		review.setTitle(title);
		review.setText(text);

		
		reviewDao.save(review);
		return "redirect:/homePage";
	}
	
	
}
