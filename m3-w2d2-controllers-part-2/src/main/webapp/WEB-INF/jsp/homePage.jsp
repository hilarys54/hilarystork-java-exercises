<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="common/header.jsp"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>

    <%
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");    
        request.setAttribute("formatter", formatter);
    %>

<c:url var="forDummiesImgSrc" value="/img/forDummies.png" />
		<img src="${forDummiesImgSrc}">


<h3>Reviews</h3>
	
	<a href="reviewInput">Submit a New Review</a>
		<div>
			<c:forEach var="review" items="${reviewList}">
				<div>
					<p><b><c:out value="${review.title}" /></b> (<c:out value="${review.username}" />)</p>
					<c:out value="${formatter.format(review.dateSubmitted)}" />
	
					
					<img src="img/stars/${review.rating}.png" style="width: 100px;" />
					<p><c:out value="${review.text}" /></p>
				</div>
				<hr>
			</c:forEach>
		</div>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />