<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="common/header.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Review Input</title>
</head>
<body>

	<div>
		<h3 class = "h3">New Review</h3>
		
			<c:url var="reviewResult" value="/reviewInput" /></c>
			<form action="${formResult}" method="POST">
				<div>
					<label for="username">Username </label>
					<input type="text" name="username" id="username" />
				</div>
				<div>
					<label for="rating">Rating </label>
					<br>
					<input type="radio" name="rating" value="1"><img src="img/stars/1.png" style="width: 100px;" />
					<br>
					<input type="radio" name="rating" value="2" /><img src="img/stars/2.png" style="width: 100px;" />
					<br>
					<input type="radio" name="rating" value="3" /><img src="img/stars/3.png" style="width: 100px;" />
					<br>
					<input type="radio" name="rating" value="4" /><img src="img/stars/4.png" style="width: 100px;" />
					<br>
					<input type="radio" name="rating" value="5" /><img src="img/stars/5.png" style="width: 100px;" />
				</div>	
				<div>		
					<label for="title">Review Title </label>
					<input type="text" name="title" id="title" />
				</div>	
				<div>		
					<label for="text">Review Text </label>
					<input type="text" name="text" id="text" style="height:200px;width:200px;"/>
				</div>	
				
				<div>
					<center><input type="submit" value="Submit!" /></center>
				</div>		
			</form>
			
		</div>

</body>
</html>

<%@include file="common/footer.jsp"%>