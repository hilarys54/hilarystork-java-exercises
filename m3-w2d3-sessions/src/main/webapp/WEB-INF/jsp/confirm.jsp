<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Favorite Season</title>
	</head>
	<body>
		<H1>Favorite Things Exercise</H1>
		<b>Favorite Color: </b>${favorites.color}
		<br>
		<b>Favorite Food: </b>${favorites.food}
		<br>
		<b>Favorite Season: </b>${favorites.season}
	</body>
</html>