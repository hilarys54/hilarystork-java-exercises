<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Favorite Color</title>
	</head>
	<body>
		<H1>Favorite Things Exercise</H1>
		<form method="POST" action="page1">
			<label for="color">What is your favorite color?</label>
			<input type="text" name="color" id="color" />
			<input type="submit" value="Next page">
		</form>
	</body>
</html>