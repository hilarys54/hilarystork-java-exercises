<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Favorite Season</title>
	</head>
	<body>
		<H1>Favorite Things Exercise</H1>
		<form method="POST" action="page3">
			<label for="season">What is your favorite season?</label>
			<input type="radio" name="season" value="Spring">Spring</radio>
			<input type="radio" name="season" value="Summer">Summer</radio>
			<input type="radio" name="season" value="Fall">Fall</radio>
			<input type="radio" name="season" value="Winter">Winter</radio>
			<input type="submit" value="Next page">
		</form>
	</body>
</html>