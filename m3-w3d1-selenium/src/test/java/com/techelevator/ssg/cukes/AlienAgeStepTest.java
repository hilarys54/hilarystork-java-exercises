package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.AgeCalculator;
import com.techelevator.ssg.pageobject.AgeCalculatorResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


@Component
public class AlienAgeStepTest {
	
	private WebDriver webDriver;
	private AgeCalculator ageCalculator;
	private AgeCalculatorResult ageCalculatorResult;
	
	@Autowired
	public AlienAgeStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.ageCalculator = new AgeCalculator(webDriver);
		this.ageCalculatorResult = new AgeCalculatorResult(webDriver);
	}
	
	@Given("^I am on the Alien Age Calculator page$")
	public void go_to_the_alien_age_calculator_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienAgeInput");
		ageCalculator = new AgeCalculator(webDriver);
	}
	
	@Given("^the planet is (.*)")
	public void the_planet_is(String planetName) throws Throwable {
		ageCalculator.choosePlanet(planetName);
	}
	
	@Given("^the Earth age is (.*)")
	public void the_Earth_age_is(String earthAge) throws Throwable {
		ageCalculator.enterAge(earthAge);
	}
	
	@When("^I request to calculate age$")
	public void i_request_to_calculate_age() throws Throwable {
		ageCalculator.submitForm();
	}
	
	@Then("^the age is (.*)$")
	public void the_age_is(float ageResult) throws Throwable {
		Assert.assertEquals(ageResult, ageCalculatorResult.getAgeResult(), 0.005);
	}
}
