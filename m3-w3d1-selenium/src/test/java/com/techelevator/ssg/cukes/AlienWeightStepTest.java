package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.WeightCalculator;
import com.techelevator.ssg.pageobject.WeightCalculatorResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Component
public class AlienWeightStepTest {
	
	private WebDriver webDriver;
	private WeightCalculator weightCalculator;
	private WeightCalculatorResult weightCalculatorResult;
	
	@Autowired
	public AlienWeightStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.weightCalculator = new WeightCalculator(webDriver);
		this.weightCalculatorResult = new WeightCalculatorResult(webDriver);
	}

	@Given("^I am on the Alien Weight Calculator page$")
	public void go_to_the_alien_weight_calculator_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienWeightInput");
		weightCalculator = new WeightCalculator(webDriver);
	}
	
	@Given("^the planet name is (.*)")
	public void the_planet_name_is(String planetName) throws Throwable {
		weightCalculator.choosePlanet(planetName);
	}
	
	@Given("^the Earth weight is (.*)")
	public void the_Earth_weight_is(String weight) throws Throwable {
		weightCalculator.enterEarthWeight(weight);
	}
	
	@When("^I request to calculate weight$")
	public void i_request_to_calculate_weight() throws Throwable {
		weightCalculator.submitForm();
	}
	
	@Then("^the weight is (.*)$")
	public void the_weight_is(float weightResult) throws Throwable {
		Assert.assertEquals(weightResult, weightCalculatorResult.getWeightResult(), 0.005);
	}
}
