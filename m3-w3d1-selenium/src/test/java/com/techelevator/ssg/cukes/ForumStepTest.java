package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.SpaceForum;
import com.techelevator.ssg.pageobject.SpaceForumResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Component
public class ForumStepTest {
	
	private WebDriver webDriver;
	private SpaceForum spaceForum;
	private SpaceForumResult spaceForumResult;
	
	@Autowired
	public ForumStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.spaceForum = new SpaceForum(webDriver);
		this.spaceForumResult = new SpaceForumResult(webDriver);
	}
	
	@Given("^I am on the Space Forum page$")
	public void go_to_the_space_forum_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/spaceForumInput");
		spaceForum = new SpaceForum(webDriver);
	}
	
	@Given("^the Username is (.*)$")
	public void the_username_is(String userName) throws Throwable {
		spaceForum.enterUsername(userName);
	}
	
	@Given("^the Subject is (.*)$")
	public void the_subject_is(String subject) throws Throwable {
		spaceForum.enterSubject(subject);
	}
	
	@Given("^the Message is (.*)$")
	public void the_message_is(String message) throws Throwable {
		spaceForum.enterMessage(message);
	}
	
	@When("^I request to submit$")
	public void i_request_to_submit() throws Throwable {
		spaceForum.submitForm();
	}
	
	@Then("^the message subject is (.*)$")
	public void the_message_result_is(String message) throws Throwable {
		WebElement lastPost = webDriver.findElement(By.cssSelector(".post:last-child > h4"));
		Assert.assertEquals(message, lastPost.getText());
	}

}
