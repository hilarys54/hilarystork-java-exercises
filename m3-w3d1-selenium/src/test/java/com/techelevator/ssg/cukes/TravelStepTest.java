package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import com.techelevator.ssg.pageobject.AgeCalculator;
import com.techelevator.ssg.pageobject.TravelCalculator;
import com.techelevator.ssg.pageobject.TravelCalculatorResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TravelStepTest {
	
	private WebDriver webDriver;
	private TravelCalculator travelCalculator;
	private TravelCalculatorResult travelCalculatorResult;
	
	@Autowired
	public TravelStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.travelCalculator = new TravelCalculator(webDriver);
		this.travelCalculatorResult = new TravelCalculatorResult(webDriver);
	}
	
	@Given("^I am on the Alien Travel Calculator page$")
	public void go_to_the_alien_travel_calculator_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienTravelInput");
		travelCalculator = new TravelCalculator(webDriver);
	}
	
	@Given("^the name of the planet is (.*)")
	public void the_name_of_the_planet_is(String planetName) throws Throwable {
		travelCalculator.choosePlanet(planetName);
	}
	
	@Given("^the mode of transportation is (.*)")
	public void the_mode_of_transportation_is(String transportation) throws Throwable {
		travelCalculator.chooseTransportation(transportation);
	}
	
	@Given("^the age on Earth is (.*)")
	public void the_age_on_earth_is(String earthAge) throws Throwable {
		travelCalculator.enterAge(earthAge);
	}
	
	@When("^I request to calculate travel time$")
	public void i_request_to_calculate_travel_time() throws Throwable {
		travelCalculator.submitForm();
	}
	
	@Then("^the time result is (.*)$")
	public void the_time_result_is(float timeResult) throws Throwable {
		Assert.assertEquals(timeResult, travelCalculatorResult.getTravelCalculatorTimeResult(), 0.005);
	}
	
	@Then("^the age result is (.*)$")
	public void the_age_result_is(float ageResult) throws Throwable {
		Assert.assertEquals(ageResult, travelCalculatorResult.getTravelCalculatorAgeResult(), 0.005);
	}

}
