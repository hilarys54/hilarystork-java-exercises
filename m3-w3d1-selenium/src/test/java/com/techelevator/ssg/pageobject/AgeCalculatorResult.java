package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AgeCalculatorResult {
	private WebDriver webDriver;
	
	public AgeCalculatorResult(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public float getAgeResult() {
		//If you are 1.0 years old on planet Earth, then you are 4 mercury years old.
		WebElement result = webDriver.findElement(By.cssSelector("#main-content"));
		String [] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[12]);
		return value;
	}

}
