package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SpaceForum implements PageObject {
private WebDriver webDriver;
	
	public SpaceForum(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public String getPageTitle() {
		WebElement title = webDriver.findElement(By.cssSelector("#main-content > h3"));
		return title.getText();
	}
	
	public SpaceForum enterUsername(String userName) {
		WebElement nameField = webDriver.findElement(By.name("username"));
		nameField.sendKeys(userName);
		return this;
	}
	
	public SpaceForum enterSubject(String subject) {
		WebElement subjectField = webDriver.findElement(By.name("subject"));
		subjectField.sendKeys(subject);
		return this;
	}
	
	public SpaceForum enterMessage(String message) {
		WebElement messageField = webDriver.findElement(By.name("message"));
		messageField.sendKeys(message);
		return this;
	}
	
	public SpaceForumResult submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='main-content']/form/div[4]/center/input"));
		submitButton.click();
		return new SpaceForumResult(webDriver);
	}	
}
