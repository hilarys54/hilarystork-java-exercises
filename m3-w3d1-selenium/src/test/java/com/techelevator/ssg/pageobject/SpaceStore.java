package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SpaceStore implements PageObject {
private WebDriver webDriver;
	
	public SpaceStore(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public String getPageTitle() {
		WebElement title = webDriver.findElement(By.cssSelector("#main-content > h3"));
		return title.getText();
	}

}
