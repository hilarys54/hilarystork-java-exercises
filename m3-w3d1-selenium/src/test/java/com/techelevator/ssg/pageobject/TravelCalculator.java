package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TravelCalculator implements PageObject {
private WebDriver webDriver;
	
	public TravelCalculator(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public String getPageTitle() {
		WebElement title = webDriver.findElement(By.cssSelector("#main-content > h3"));
		return title.getText();
	}
	
	public TravelCalculator choosePlanet(String planetName) {
		Select planetSelection = new Select(webDriver.findElement(By.name("planet")));
		planetSelection.selectByVisibleText(planetName);
		return this;
	}
	
	public TravelCalculator chooseTransportation(String transportation) {
		Select transportationSelection = new Select(webDriver.findElement(By.name("transportationMode")));
		transportationSelection.selectByVisibleText(transportation);
		return this;
	}
	
	public TravelCalculator enterAge(String age) {
		WebElement ageField = webDriver.findElement(By.name("earthAge"));
		ageField.sendKeys(age);
		return this;
	}
	
	public TravelCalculatorResult submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='main-content']/form/table/tbody/tr[4]/td[2]/input"));
		submitButton.click();
		return new TravelCalculatorResult(webDriver);
	}	
}
