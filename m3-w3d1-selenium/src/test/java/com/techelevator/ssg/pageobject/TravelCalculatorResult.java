package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TravelCalculatorResult {
	
private WebDriver webDriver;
	
	public TravelCalculatorResult(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public float getTravelCalculatorTimeResult() {
		//Traveling by walking you will reach mercury in 1189.0699010654491 years. You will be 1219.0699010654491 years old.
		WebElement result = webDriver.findElement(By.cssSelector("#main-content"));
		String [] parts = result.getText().split(" ");
		float timeValue = Float.parseFloat(parts[8]);
		return timeValue;
	}
	
	public float getTravelCalculatorAgeResult() {
		//Traveling by walking you will reach mercury in 1189.0699010654491 years. You will be 1219.0699010654491 years old.
		WebElement result = webDriver.findElement(By.cssSelector("#main-content"));
		String [] parts = result.getText().split(" ");
		float ageValue = Float.parseFloat(parts[13]);
		return ageValue;
	}

}
