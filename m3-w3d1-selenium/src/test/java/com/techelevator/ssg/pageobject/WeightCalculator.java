package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class WeightCalculator implements PageObject {
	private WebDriver webDriver;
	
	public WeightCalculator(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public String getPageTitle() {
		WebElement title = webDriver.findElement(By.cssSelector("#main-content > h3"));
		return title.getText();
	}
	
	public WeightCalculator choosePlanet(String planetName) {
		Select planetSelection = new Select(webDriver.findElement(By.name("planet")));
		planetSelection.selectByVisibleText(planetName);
		return this;
	}
	
	public WeightCalculator enterEarthWeight(String weight) {
		WebElement weightField = webDriver.findElement(By.name("earthWeight"));
		weightField.sendKeys(weight);
		return this;
	}
	
	public WeightCalculatorResult submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='main-content']/form/table/tbody/tr[3]/td[2]/input"));
		submitButton.click();
		return new WeightCalculatorResult(webDriver);
	}

}
