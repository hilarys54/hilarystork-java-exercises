package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
	
public class WeightCalculatorResult {
	private WebDriver webDriver;
	
	public WeightCalculatorResult(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public float getWeightResult() {
		//If you are 100.0 lbs on planet Earth, you would weigh 37 lbs on mercury.	
		//If you are 1.0 years old on planet Earth, then you are 4 mercury years old.
		WebElement result = webDriver.findElement(By.cssSelector("#main-content"));
		String [] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[11]);
		return value;
	}

}

