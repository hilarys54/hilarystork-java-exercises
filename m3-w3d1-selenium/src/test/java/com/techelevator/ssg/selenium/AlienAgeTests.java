package com.techelevator.ssg.selenium;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.ssg.pageobject.AgeCalculator;
import com.techelevator.ssg.pageobject.AgeCalculatorResult;



public class AlienAgeTests {
	
	private static WebDriver webDriver;
	private AgeCalculator ageCalculator;
	
	@BeforeClass
	public static void setUpWebDriver() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver = new ChromeDriver();	
	}
	
	@AfterClass
	public static void cleanUpWebDriver() {
		webDriver.close();
	}
	
	@Before
	public void setup() {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienAgeInput");
		ageCalculator = new AgeCalculator(webDriver);
	}
	
	@Test
	public void one_hundred_earth_years_on_mercury_is_415() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Mercury")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(415f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_years_on_venus_is_162() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Venus")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(162f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_years_on_earth_is_100() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Earth")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(100f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_years_on_mars_is_53() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Mars")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(53f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_years_on_jupiter_is_8() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Jupiter")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(8f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_years_on_saturn_is_3() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Saturn")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(3f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_years_on_uranus_is_1() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Uranus")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(1f, result.getAgeResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_year_on_neptune_is_1() {
		AgeCalculatorResult result = ageCalculator.choosePlanet("Neptune")
													.enterAge("100")
													.submitForm();
		Assert.assertEquals(1f, result.getAgeResult(), 0.005);
	}	
	
}
