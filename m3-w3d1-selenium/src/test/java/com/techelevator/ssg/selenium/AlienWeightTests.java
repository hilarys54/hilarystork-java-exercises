package com.techelevator.ssg.selenium;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.ssg.pageobject.WeightCalculator;
import com.techelevator.ssg.pageobject.WeightCalculatorResult;



public class AlienWeightTests {

	private static WebDriver webDriver;
	private WeightCalculator weightCalculator;
	
	@BeforeClass
	public static void setUpWebDriver() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver = new ChromeDriver();	
	}
	
	@AfterClass
	public static void cleanUpWebDriver() {
		webDriver.close();
	}
	
	@Before
	public void setup() {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienWeightInput");
		weightCalculator = new WeightCalculator(webDriver);
	}
	
	@Test
	public void one_hundred_earth_pounds_on_mercury_is_37() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Mercury")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(37f,  result.getWeightResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_pounds_on_venus_is_90() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Venus")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(90f,  result.getWeightResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_pounds_on_earth_is_100() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Earth")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(100f,  result.getWeightResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_pounds_on_mars_is_38() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Mars")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(38f,  result.getWeightResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_pounds_is_on_jupiter_is_265() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Jupiter")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(265f,  result.getWeightResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_pounds_on_saturn_is_113() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Saturn")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(113f,  result.getWeightResult(), 0.005);
	}
	@Test
	public void one_hundred_earth_pounds_on_uranus_is_109() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Uranus")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(109f,  result.getWeightResult(), 0.005);
	}
	
	@Test
	public void one_hundred_earth_pounds_on_neptune_is_143() {
		WeightCalculatorResult result = weightCalculator.choosePlanet("Neptune")
														.enterEarthWeight("100")
														.submitForm();
		Assert.assertEquals(143f,  result.getWeightResult(), 0.005);
	}
}
