package com.techelevator.ssg.selenium;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.ssg.pageobject.TravelCalculator;
import com.techelevator.ssg.pageobject.TravelCalculatorResult;
import com.techelevator.ssg.pageobject.WeightCalculator;



public class DriveTimeTests {
	private static WebDriver webDriver;
	private TravelCalculator travelCalculator;
	
	@BeforeClass
	public static void setUpWebDriver() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver = new ChromeDriver();	
	}
	
	@AfterClass
	public static void cleanUpWebDriver() {
		webDriver.close();
	}
	
	@Before
	public void setup() {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienTravelInput");
		travelCalculator = new TravelCalculator(webDriver);
	}
	
	@Test
	public void mercury_walking_10_is_time_result_1189_is_age_result_1199() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Mercury")
												.chooseTransportation("Walking")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(1189.070f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(1199.070f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void mars_walking_10_is_time_result_1852_is_age_result_1862() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Mars")
												.chooseTransportation("Walking")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(1852.291f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(1862.291f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void venus_car_10_is_time_result_29_is_age_result_39() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Venus")
												.chooseTransportation("Car")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(29.366f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(39.366f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void jupiter_car_10_is_time_result_446_is_age_result_456() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Jupiter")
												.chooseTransportation("Car")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(445.976f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(455.976f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void earth_bullet_train_10_is_time_result_0_is_age_result_10() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Earth")
												.chooseTransportation("Bullet Train")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(0.0f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(10.0f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void saturn_bullet_train_10_is_time_result_452_is_age_result_462() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Saturn")
												.chooseTransportation("Bullet Train")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(452.197f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(462.197f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void mars_boeing_10_is_time_result_10_is_age_result_20() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Mars")
												.chooseTransportation("Boeing 747")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(9.749f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(19.749f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void uranus_boeing_10_is_time_result_339_is_age_result_349() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Uranus")
												.chooseTransportation("Boeing 747")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(338.994f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(348.994f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void jupiter_concorde_10_is_time_result_33_is_age_result_43() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Jupiter")
												.chooseTransportation("Concorde")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(33.035f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(43.035f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	@Test
	public void mercury_concorde_10_is_time_result_3_is_age_result_13() {
		TravelCalculatorResult result = travelCalculator.choosePlanet("Mercury")
												.chooseTransportation("Concorde")
												.enterAge("10")
												.submitForm();
		Assert.assertEquals(2.642f, result.getTravelCalculatorTimeResult(), 0.005);	
		Assert.assertEquals(12.642f, result.getTravelCalculatorAgeResult(), 0.005);
	}
	
	
}
