package com.techelevator.ssg.selenium;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.ssg.pageobject.SpaceForum;
import com.techelevator.ssg.pageobject.SpaceForumResult;
import com.techelevator.ssg.pageobject.WeightCalculator;


public class ForumTests {
	
	private static WebDriver webDriver;
	private SpaceForum spaceForum;
	
	@BeforeClass
	public static void setUpWebDriver() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver = new ChromeDriver();	
	}
	
	@AfterClass
	public static void cleanUpWebDriver() {
		webDriver.close();
	}
	
	@Before
	public void setup() {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/spaceForumInput");
		spaceForum = new SpaceForum(webDriver);
	}
	
	@Test 
	public void verify_last_post() {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/spaceForumResult");
		WebElement lastPost = webDriver.findElement(By.cssSelector(".post:last-child > h4"));
		Assert.assertNotNull(lastPost);
		Assert.assertEquals(lastPost.getText(), "test post");
	}
	
//	@Test
//	public void submit_a_post_works() {
//		SpaceForumResult result = spaceForum.enterUsername("spaceNerd")
//											.enterSubject("Message")
//											.enterMessage("This is a message.")
//											.submitForm();
//		Assert.assertEquals("This is a message.", result.getMessageResult());
//	}

}
