package com.techelevator.ssg.selenium;


import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.techelevator.ssg.pageobject.AgeCalculator;
import com.techelevator.ssg.pageobject.HomePage;
import com.techelevator.ssg.pageobject.SpaceForum;
import com.techelevator.ssg.pageobject.SpaceStore;
import com.techelevator.ssg.pageobject.TravelCalculator;
import com.techelevator.ssg.pageobject.WeightCalculator;

import cucumber.api.java.lu.a;

public class HomePageTests {
	
	private static WebDriver webDriver;
	private HomePage homePage;
	
	@BeforeClass
	public static void setUpWebDriver() {
		String homeDir = System.getProperty("user.home");
		System.setProperty("webdriver.chrome.driver", homeDir+"/dev-tools/chromedriver/chromedriver");
		webDriver = new ChromeDriver();	
	}
	
	@AfterClass
	public static void cleanUpWebDriver() {
		webDriver.close();
	}
	
	@Before
	public void setup() {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/");
		homePage = new HomePage(webDriver);
	}
	
	@Test
	public void verify_there_are_eight_planets() {
		List<WebElement> planets = homePage.getPlanetElements();
		
		Assert.assertNotNull(planets);
		Assert.assertEquals(8, planets.size());
	}
	
	@Test
	public void verify_planet_names() {
		List<WebElement> planetElements = homePage.getPlanetNameElements();
		List<String> planetNames = new ArrayList<String>();
		
		for(WebElement element : planetElements) {
			planetNames.add(element.getText());
		}
		
		Assert.assertTrue("Mercury was not in the list", planetNames.contains("Mercury"));
		Assert.assertTrue("Venus was not in the list", planetNames.contains("Venus"));
		Assert.assertTrue("Earth was not in the list", planetNames.contains("Earth"));
		Assert.assertTrue("Mars was not in the list", planetNames.contains("Mars"));
		Assert.assertTrue("Jupiter was not in the list", planetNames.contains("Jupiter"));
		Assert.assertTrue("Saturn was not in the list", planetNames.contains("Saturn"));
		Assert.assertTrue("Uranus was not in the list", planetNames.contains("Uranus"));
		Assert.assertTrue("Neptune was not in the list", planetNames.contains("Neptune"));
	}
	
	@Test
	public void verify_age_calculator_link() {
		AgeCalculator ageCalculator = homePage.clickAgeCalculatorLink();
		
		Assert.assertEquals("Alien Age Calculator", ageCalculator.getPageTitle());
	}
	
	@Test
	public void verify_weight_calculator_link() {
		WeightCalculator weightCalculator = homePage.clickWeightCalculatorLink();
		
		Assert.assertEquals("Alien Weight Calculator", weightCalculator.getPageTitle());
	}
	
	
	@Test
	public void verify_travel_calculator_link() {
		TravelCalculator travelCalculator = homePage.clickTravelCalculatorLink();
		
		Assert.assertEquals("Alien Travel Calculator", travelCalculator.getPageTitle());
	}
	
	@Test
	public void verify_space_forum_link() {
		SpaceForum spaceForum = homePage.clickSpaceForumLink();
		
		Assert.assertEquals("New Geek Post", spaceForum.getPageTitle());
	}
	
	@Test
	public void verify_space_store_link() {
		SpaceStore spaceStore = homePage.clickSpaceStoreLink();
		
		Assert.assertEquals("Solar System Geek Gift Shop", spaceStore.getPageTitle());
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
