Feature: Alien Age

Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Mercury
	And the Earth age is 100
	When I request to calculate age
	Then the age is 415
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Venus
	And the Earth age is 100
	When I request to calculate age
	Then the age is 162
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Earth
	And the Earth age is 100
	When I request to calculate age
	Then the age is 100
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Mars
	And the Earth age is 100
	When I request to calculate age
	Then the age is 53
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Jupiter
	And the Earth age is 100
	When I request to calculate age
	Then the age is 8
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Saturn
	And the Earth age is 100
	When I request to calculate age
	Then the age is 3
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Uranus
	And the Earth age is 100
	When I request to calculate age
	Then the age is 1
	
Scenario: Click on calculate age
	Given I am on the Alien Age Calculator page
	And the planet is Neptune
	And the Earth age is 100
	When I request to calculate age
	Then the age is 1