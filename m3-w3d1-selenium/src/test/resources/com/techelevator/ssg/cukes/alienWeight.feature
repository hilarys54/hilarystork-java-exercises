Feature: Alien Weight

Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Mercury
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 37
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Venus
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 90
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Earth
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 100
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Mars
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 38
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Jupiter
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 265
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Saturn
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 113
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Uranus
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 109
	
Scenario: Click on calculate weight
	Given I am on the Alien Weight Calculator page
	And the planet name is Neptune
	And the Earth weight is 100
	When I request to calculate weight
	Then the weight is 143