Feature: Home Page

Scenario: Click on the age calculator
	Given I am on the home page
	And I click on the link: Alien Age
	Then the title will be Alien Age Calculator
	
Scenario: Click on the weight calculator
	Given I am on the home page
	And I click on the link: Alien Weight
	Then the title will be Alien Weight Calculator
	
Scenario: Click on the travel calculator
	Given I am on the home page
	And I click on the link: Drive Time
	Then the title will be Alien Travel Calculator
	
Scenario: Click on the space forum
	Given I am on the home page
	And I click on the link: Space Forum
	Then the title will be New Geek Post
	
Scenario: Click on the space store
	Given I am on the home page
	And I click on the link: Space Store
	Then the title will be Solar System Geek Gift Shop