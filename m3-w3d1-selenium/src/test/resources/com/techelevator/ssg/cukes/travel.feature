Feature: Travel

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Mercury
	And the mode of transportation is Walking
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 1189.070 
	And the age result is 1199.070

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Mars
	And the mode of transportation is Walking
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 1852.291 
	And the age result is 1862.291

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Venus
	And the mode of transportation is Car
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 29.366 
	And the age result is 39.366

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Jupiter
	And the mode of transportation is Car
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 445.976
	And the age result is 455.976

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Earth
	And the mode of transportation is Bullet Train
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 0.0 
	And the age result is 10.0

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Saturn
	And the mode of transportation is Bullet Train
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 452.197 
	And the age result is 462.197

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Mars
	And the mode of transportation is Boeing 747
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 9.749 
	And the age result is 19.749

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Uranus
	And the mode of transportation is Boeing 747
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 338.994 
	And the age result is 348.994

Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Jupiter
	And the mode of transportation is Concorde
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 33.035 
	And the age result is 43.035
	
Scenario: Click on calculate travel time
	Given I am on the Alien Travel Calculator page
	And the name of the planet is Mercury
	And the mode of transportation is Concorde
	And the age on Earth is 10
	When I request to calculate travel time
	Then the time result is 2.642 
	And the age result is 12.642
	
	