$(document).ready(function() {
	//1.
	var recipeChildren = $("#recipes").children();
	console.log(recipeChildren);
	
	//2.
	var beefTitles = $(".beef .title");
	console.log(beefTitles);
	
	//3.
	var catfishChildren = $("#blackened-catfish").children();
	console.log(catfishChildren);
	
	//4. 
	var saladParent = $("#winter-fruit-salad").parent();
	console.log(saladParent);
	
	//5.
	var tilapiaParents = $("#curried-tilapia").parents();
	console.log(tilapiaParents);
	
	//6.
	var chiliMadeSiblings = $("#tailgate-chili .made-it").siblings();
	console.log(chiliMadeSiblings);
	
	//7.
	var vegetarianSiblings = $(".vegetarian").siblings();
	console.log(vegetarianSiblings);
	
	//8.
	var evenListItems = $("li").filter(":even");
	console.log(evenListItems);
	
	//9.
	var burritoChildren = $("#black-bean-burrito").children();
	var madeItChildren = $(burritoChildren).filter(".made-it");
	console.log(madeItChildren);
	
	//10.
	var h3Text = $("h3").text();
	console.log(h3Text);
	
	//11.
	$("#cabbage-pie > h3").text("Homemade Cabbage Pie -- Yum!");
	
	//12.
	$("#cabbage-pie > h3").html("Homemade Cabbage Pie <br> Yum!");
	
	//13.
	$("#cabbage-pie > h3").text("Homemade Cabbage Pie <br> Yum!");
	
	//14.
	$("p").text("Delicious recipe description and instructions go here.");
	
	//15.
	var remarksType = $(".remarks").attr("type");
	console.log(remarksType);
	
	//16.
	$(".remarks").attr("type", "text");
	
	//17.
	$(".remarks").attr("value", "Your remarks go here.");
	
	//18.
	$(".made-it").attr("checked", "checked");
	
	//19.
	$("ul").append("<li>Poultry</li>");
	
	
	
});