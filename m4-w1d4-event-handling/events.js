$(document).ready(function() {
	//1
	$("#deepSkyBox").click(function(event) {
		alert("Hey, you clicked on me!");
	});
	
	//2 & 3
	$("#box-rainbow").children().dblclick(function() {
		if($(this).hasClass("selected")) {
			$(this).removeClass("selected");
		} else {
			$(this).addClass("selected");
		}
	});
	
	//4
	$("#hideSelectedBoxes").click(function() {
		$(".selected").hide();	
	});
	
	//5
	$("#showAllBoxes").click(function() {
		$(".selected").show();	
	});
	
	//6
	$("#refreshSelectedBoxList").click(function() {
		$("#selectedBoxList").empty();
		$(".selected > p").each(function() {
			$("#selectedBoxList").append("<li>" + $(this).text() + "</li>");
		});
	});

	//7. Add a border using the css-class `.selected-field` to the textboxes on the page when they receive focus. Remove the border when the textbox loses focus.
	
	$(":text").focus(function() {
		$(this).addClass("selected-field");
	});
	
	//8. In the Form Address section, when the user indicates that they have a different billing address by checking the "My billing address is different box", display the fields in the "billingAddress" box. Hide those fields again if the user unchecks it.  
	$("#differentBillingAddress").click(function() {
		if (this.checked) {
			$("#billingAddress").show();
		} else {
			$("#billingAddress").hide();
		}
	});
	
	//9. As text is typed into the repeating text box display it out to the paragraph tag (#txtFieldOutput) element below it.
	
	$("#txtField").keyup(function(event) {
		$("#txtFieldOutput").html($(this).val());
	});
	
	//10. Add an event that when the "Mark all as checked" checkbox is checked, all of the sub-checkboxes are also checked.  If the user unchecks it, all checkboxes below it are unchecked. 
	
	$("#checkAll").change(function() {
		if(this.checked) {
			$(".subcheck").prop("checked", true);
		} else {
			$(".subcheck").prop("checked", false);
		}
	});
	
	//11. **Challenge** Make the swap buttons work. If the boxes in the 2-4 position are swapped, switch places with the box immediately preceding it (i.e. 3 is clicked therefore 3 and 2 swap places). If boxes in positions 5-7 are clicked swap places with the box immediately following it. For box 1 switches places with the box immediately following, and box 8 switch places with the box immediately preceding.
	
	$(".box > button").click(function() {
		if($(this).text() == "<--Swap") {
			var originalClass = $(this).parent().attr("class");
			var newClass = $(this).parent().prev().attr("class");
			$(this).parent().removeClass(originalClass).addClass(newClass);
			$(this).parent().prev().removeClass(newClass).addClass(originalClass);
		} else {
			var originalClass = $(this).parent().attr("class");
			var newClass = $(this).parent().next().attr("class");
			$(this).parent().removeClass(originalClass).addClass(newClass);
			$(this).parent().next().removeClass(newClass).addClass(originalClass);
		}
	});	
});