function getInfo() {

	$.ajax({
		url: 'https://crossorigin.me/https://www.opentdb.com/api.php?amount=10',
		method: "GET",
		dataType: 'json'
	})
	.done(function(data) {
		console.log(data);
		var table = $("#trivia");
		for(var i=1; i < data.results.length; i++) {
			question = data.results[i];
			
			var tableRow = $("<tr>");
		
			var trivia = $("#quizQuestions").html(question.question + "<br>" + question.correct_answer + question.incorrect_answers);
			var triviaCell = $("<td>").append(trivia);
			tableRow.append(triviaCell);
			
			table.append(tableRow);
		}	
		
	})
	
	.fail(function(xhr, status, error) {
		console.log(error);
	});
}

$(document).ready(getInfo);
