function getInfo() {

	$.ajax({
		url: "https://randomuser.me/api/?results=10",
		method: "GET",
		dataType: "json"
	})
	.done(function(data) {
		console.log(data);
		$("#otherUsers").empty();
		var user = data.results[0];
		
		//Fill out main profile
		$("#user-photo").attr("src", user.picture.large);
		$("#name").text(user.name.first + " " + user.name.last);
		$("#fullName").text(user.name.title + ". " + user.name.first + " " + user.name.last);
		var dob = new Date(user.dob);
		
		var numberMonth = dob.getMonth();
		var wordMonth;
		if(numberMonth == 0) {
			wordMonth = "Jan";	
		} else if(numberMonth == 1) {
			wordMonth = "Feb";
		} else if(numberMonth == 2) {
			wordMonth = "March";
		} else if(numberMonth == 3) {
			wordMonth = "April";
		} else if(numberMonth == 4) {
			wordMonth = "May";
		} else if(numberMonth == 5) {
			wordMonth = "June";
		} else if(numberMonth == 6) {
			wordMonth = "July";
		} else if(numberMonth == 7) {
			wordMonth = "Aug";
		} else if(numberMonth == 8) {
			wordMonth = "Sept";
		} else if(numberMonth == 9) {
			wordMonth = "Oct";
		} else if(numberMonth == 10) {
			wordMonth = "Nov";
		} else if(numberMonth == 11) {
			wordMonth = "Dec";
		}
		
		var numberDay = dob.getDay();
		var wordDay;
		if(numberDay == 0) {
			wordDay = "Sun";
		} else if(numberDay == 1) {
			wordDay = "Mon";
		} else if(numberDay == 2) {
			wordDay = "Tues";
		} else if(numberDay == 3) {
			wordDay = "Weds";
		} else if(numberDay == 4) {
			wordDay = "Thurs";
		} else if(numberDay == 5) {
			wordDay = "Fri";
		} else if(numberDay == 6) {
			wordDay = "Sat";
		}
		
		var date = dob.getDate();
		var year = dob.getFullYear();
		
		var formattedDOBDate = wordDay + " " + wordMonth + " " + date + ", " + year;
		$("#dob").text(formattedDOBDate);
		
		var joined = new Date(user.registered);
		var joinedNumberMonth = joined.getMonth();
		var joinedWordMonth;
		if(joinedNumberMonth == 0) {
			joinedWordMonth = "Jan";	
		} else if(joinedNumberMonth == 1) {
			joinedWordMonth = "Feb";
		} else if(joinedNumberMonth == 2) {
			joinedWordMonth = "March";
		} else if(joinedNumberMonth == 3) {
			joinedWordMonth = "April";
		} else if(joinedNumberMonth == 4) {
			joinedWordMonth = "May";
		} else if(joinedNumberMonth == 5) {
			joinedWordMonth = "June";
		} else if(joinedNumberMonth == 6) {
			joinedWordMonth = "July";
		} else if(joinedNumberMonth == 7) {
			joinedWordMonth = "Aug";
		} else if(joinedNumberMonth == 8) {
			joinedWordMonth = "Sept";
		} else if(joinedNumberMonth == 9) {
			joinedWordMonth = "Oct";
		} else if(joinedNumberMonth == 10) {
			joinedWordMonth = "Nov";
		} else if(joinedNumberMonth == 11) {
			joinedWordMonth = "Dec";
		}
		
		var joinedNumberDay = joined.getDay();
		var joinedWordDay;
		if(joinedNumberDay == 0) {
			joinedWordDay = "Sun";
		} else if(joinedNumberDay == 1) {
			joinedWordDay = "Mon";
		} else if(joinedNumberDay == 2) {
			joinedWordDay = "Tues";
		} else if(joinedNumberDay == 3) {
			joinedWordDay = "Weds";
		} else if(joinedNumberDay == 4) {
			joinedWordDay = "Thurs";
		} else if(joinedNumberDay == 5) {
			joinedWordDay = "Fri";
		} else if(joinedNumberDay == 6) {
			joinedWordDay = "Sat";
		}
		
		var joinedDate = joined.getDate();
		var joinedYear = joined.getFullYear();
		
		var formattedJoinedDate = joinedWordDay + " " + joinedWordMonth + " " + joinedDate + ", " + joinedYear;
		$("#joined").text(formattedJoinedDate);
		
		$("#phone").text(user.phone);
		$("#address").html(user.location.street + "<br>" + user.location.city + ", " + user.location.state + " " + user.location.postcode);
		$("#email").text(user.email);
		$("#username").text(user.login.username);
		var passwordLength = user.login.password.length;
		$("#password").text("*".repeat(passwordLength));
		
		
		//Fill out sub-profiles
		var table = $("#otherUsers");
		for(var i=1; i < data.results.length; i++) {
			user = data.results[i];
			
			var tableRow = $("<tr>");
			
			var image = $("<img>").prop("src", user.picture.thumbnail);
			var imageCell = $("<td>").append(image);
			tableRow.append(imageCell);
			
			var nameCell = $("<td>").text(user.name.first + " " + user.name.last);
			tableRow.append(nameCell);
			
			var emailCell = $("<td>").text(user.email);
			tableRow.append(emailCell);
			
			var phoneCell = $("<td>").text(user.phone);
			tableRow.append(phoneCell);
			
			table.append(tableRow);
		}
	})
	.fail(function(xhr, status, error) {
		console.log(error);
	});
}

$(document).ready(function() {
	getInfo();
	$("#refreshButton").click(getInfo);
});