$(document).ready(function() {
	$("#authentication-form").validate({
		
		debug: true,
		rules: {
			username: {
				required: true,
				email: true
			},
			password: {
				required: true
			}
		},
		messages: {
			username: {
				required: "Your Username cannot be blank.",
				email: "Your username must be a valid email address."
			},
			password: {
				required: "Your password cannot be blank."
			}	
		},
		errorClass: "error",
		validClass: "valid"
	}); 
});