$(document).ready(function() {

	$("#confidentialForm").validate({
	
		debug: true,
		rules: {
			birthday: {
				required: true,
				formattedDate: true,
			},
			ssn: {
				required: true,
				ssn: true,
			},
			address2: {
				empty: {
					depends: function(element) {
						return $("#address1").is(":blank");
					}
				}
			},
			city: {
				required: true,
			},
			state: {
				required: true,
			},
			zip: {
				required: true,
				zipCodeUS: true,	
			},
			gender: {
				required: true,
			}	
		},
		messages: {
			birthday: {
				required: "Birthdate is required.",
				isValidDate: "Please enter a valid date as mm/dd/yyyy."
			},
			ssn: {
				required: "Social Security Number is required.",
				SocialSecurity: "Please enter a valid SSN."
			},
			address2: {
				empty: "The first address line must also be filled in."
			},
			city: {
				required: "City is required."
			},
			state: {
				required: "State is required."
			},
			zip: {
				required: "Zip code is required.",
				zipCodeUS: "Zip code must me of the form 12345."
			},
			gender: {
				required: "Gender is required."
			}		
		},
		errorClass: "error",
		validClass: "valid"
		
	});
});	

$.validator.addMethod("ssn", function(value, element) {
return this.optional(element) || /^(\d{3})-\d{2}-\d{4}$/i.test(value);
}, "Invalid ssn");

$.validator.addMethod("formattedDate", function(value, element) {
return this.optional(element) || /(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20\d\d)/.test(value);
}, "Invalid date");



$.validator.addMethod('empty', function (value, element) {
      return (value === '');
  }, "This field must remain empty!");



