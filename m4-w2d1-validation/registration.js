$(document).ready(function() {
	
	$("#registrationForm").validate({
		debug: true,
		rules: {
			email: {
				required: true,
				email: true,
			},
			firstName: {
				required: true,
			},
			lastName: {
				required: true,
			},
			password: {
				required: true,
				minlength: 8,
				strongpassword: true,	
			},
			confirm: {
				equalTo: "#password",
			},
			country: {
				required: true,
			},
			homePhone: {
				required: true,
				intlphone: true,
			},
			officePhone: {
				required: true,
				intlphone: true,
			},
			cellPhone: {
				required: true,
				intlphone: true,
			},
			
				
		},
		messages: {
			email: {
				required: "Email is a required field.",
				email: "Please enter a valid email",
			},	
			firstName: {
				required: "First name is a required field."
			},
			lastName: {
				required: "Last name is a required field."
			},
			password: {
				required: "Password is a required field.",
				minlength: "The password must be at least 8 characters.",
				strongpassword: "Please enter a strong password )(one capital, one special character, and one number).",	
			},
			confirm: {
				equalTo: "The passwords don't match."
			},
			country: {
				required: "Country is a required field."
			},
			homePhone: {
				required: "Home phone is a required field.",
				intlphone: "Please enter a valid phone.",
			},
			officePhone: {
				required: "Office phone is a required field.",
				intlphone: "Please enter a valid phone.",
			},
			cellPhone: {
				required: "Cell phone is a required field.",
				intlphone: "Please enter a valid phone.",
			},
			
		},
		errorClass: "error",
		validClass: "valid"
	});
	
});

$.validator.addMethod("strongpassword", function (value, index) {
    return value.match(/[A-Z]/) && (this.optional(element) || /^\w+$/i.test(value))  && value.match(/\d/);
}, "Please enter a strong password )(one capital, one special character, and one number).");

$.validator.addMethod('intlphone', function(value) { 
	return value.match(/^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}(\s*(ext|x)\s*\.?:?\s*([0-9]+))?$/); 
}, 'Please enter a valid phone number');