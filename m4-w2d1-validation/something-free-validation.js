$(document).ready(function() {
	$("#allNewsletters").click(function() {
		var checked = $(this).prop("checked");
		$(".subcheck").prop("checked", checked);
	});
	
	$("#freeForm").validate({
		debug: true,
		rules: {
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: 8,
				strongpassword: true
			},
			confirm: {
				equalTo: "#password"
			},
			company: {
				required: true
			},
			website: {
				url: true
			},
			industry: {
				required: true
			},
			position: {
				required: true
			},
			numberOfEmployees: {
				required: true
			}
		},
		messages: {
			email: {
				required: "You must provide an email.",
				email: "You must provide a valid email.",
			},
			password: {
				required: "You must provide a password.",
				minlength: "Your passward must contain at least 8 characters.",
				strongpassword: "Please enter a strong password )(one capital, one special character, and one number).",
			},
			confirm: {
				equalTo: "This must be the same as the password above."
			},
			company: {
				required: "You must provide a company."
			},
			website: {
				url: "This must be a valid url."
			},
			industry: {
				required: "You must provide a company."
			},
			position: {
				required: "You must provide a position."
			},
			numberOfEmployees: {
				required: "You must provide a number of employees."
			}
			
		},
		errorClass: "error",
		validClass: "valid"
	});
});


$.validator.addMethod("strongpassword", function (value, index) {
    return value.match(/[A-Z]/) && (this.optional(element) || /^\w+$/i.test(value))  && value.match(/\d/);
}, "Please enter a strong password )(one capital, one special character, and one number).");